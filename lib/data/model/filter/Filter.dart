class Filter {
  Category category = Category.ALL;
  Region region = Region.ALL;
  Sort sort = Sort.ACTUAL;
  bool onlyTitle = false;

  Filter(
      {required this.category,
        required this.region,
        required this.sort,
        required this.onlyTitle});

  Filter.fromJson(Map<String, dynamic> json)
      : category = Category.values[json['category']],
        region = Region.values[json['region']],
        sort = Sort.values[json['sort']],
        onlyTitle = json['onlyTitle'];

  Map<String, dynamic> toJson() => {
    'category': category.index,
    'region': region.index,
    'sort': sort.index,
    'onlyTitle': onlyTitle
  };

  @override
  bool operator == (o) => o is Filter && category == o.category && region == o.region && sort == o.sort && onlyTitle == o.onlyTitle;

  @override
  int get hashCode => category.hashCode ^ region.hashCode ^ sort.hashCode ^ onlyTitle.hashCode;

}

enum Category {
  ALL, SELL, BUY, SWAP, SERVICE, RENT }

extension CategoryExtension on Category {
  String get text {
    switch (this) {
      case Category.ALL:
        return 'Все';
      case Category.SELL:
        return 'Продам';
      case Category.BUY:
        return 'Куплю';
      case Category.SWAP:
        return 'Обмен';
      case Category.SERVICE:
        return 'Услуга';
      case Category.RENT:
        return 'Аренда';
    }
  }
}

enum Region {
  ALL,
  MINSK,
  MINSK_REGION,
  BREST_REGION,
  VITEBSK_REGION,
  GOMEL_REGION,
  GRODNO_REGION,
  MOGILEV_REGION
}

extension RegionExtension on Region {
  String get text {
    switch(this) {
      case Region.ALL:
        return 'Вся Беларусь';
      case Region.MINSK:
        return 'Минск';
      case Region.MINSK_REGION:
        return 'Минская обл.';
      case Region.BREST_REGION:
        return 'Брестская обл.';
      case Region.VITEBSK_REGION:
        return 'Витебская обл.';
      case Region.GOMEL_REGION:
        return 'Гомельская обл.';
      case Region.GRODNO_REGION:
        return 'Гродненская обл.';
      case Region.MOGILEV_REGION:
        return 'Могилевская обл.';
    }
  }
}

enum Sort { PRICE_ASCENDING, PRICE_DESCENDING, ACTUAL, NEW }

extension SortExtension on Sort {
  String get text {
    switch(this){
      case Sort.PRICE_ASCENDING:
        return 'Цена (по возрастанию)';
      case Sort.PRICE_DESCENDING:
        return 'Цена (по убыванию)';
      case Sort.ACTUAL:
        return 'Актуальные';
      case Sort.NEW:
        return 'Новые';
    }
  }
}
class FullItem{
  final String title;
  final String description;
  final String descriptionShort;
  final List<String> srcUrls;
  final String? price;
  final bool isTorg;
  final String city;
  final String link;
  final int countUp;
  final String afterUp;
  final List<String> numbersPhone;
  final String nameAuthor;
  final String avatarAuthor;
  final String allAdsAuthor;
  final String linkWriteAuthor;

  FullItem({required this.title, required this.description, required this.descriptionShort, required this.srcUrls, this.price, required this.isTorg, required this.city, required this.link, required this.countUp, required this.afterUp, required this.numbersPhone, required this.nameAuthor, required this.avatarAuthor, required this.allAdsAuthor, required this.linkWriteAuthor});

}
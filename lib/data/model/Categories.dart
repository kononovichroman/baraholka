import 'package:flutter/material.dart';

class Categories {
  final String name;
  final IconData icon;

  Categories({required this.name, required this.icon});

  static List<Categories> initializeDataList() {
    return [
      Categories(name: "Телефоны. Смартфоны. Умные часы", icon: Icons.smartphone_outlined),
      Categories(name: "Ноутбуки. Компьютеры. Apple. Офисная техника", icon: Icons.laptop_outlined),
      Categories(name: "Фото. Аудио. Видео. Приставки. Игры", icon: Icons.camera_alt_outlined),
      Categories(name: "Авто. Мото: аренда и продажа", icon: Icons.directions_car_outlined),
      Categories(name: "Автозапчасти и Автоаксессуары", icon: Icons.car_repair_outlined),
      Categories(name: "Дом и сад", icon: Icons.house_outlined),
      Categories(name: "Бытовая техника", icon: Icons.local_laundry_service_outlined),
      Categories(name: "Строительство и ремонт", icon: Icons.construction_outlined),
      Categories(name: "Спорт. Хобби. Развлечения", icon: Icons.sports_baseball_outlined),
      Categories(name: "Работа и Бизнес", icon: Icons.work_outline),
      Categories(name: "Животные и уход за ними", icon: Icons.pets_outlined),
      Categories(name: "Недвижимость", icon: Icons.apartment_outlined),
      Categories(name: "Женская одежда и аксессуары", icon: Icons.checkroom_outlined),
      Categories(name: "Женская обувь", icon: Icons.directions_walk_outlined),
      Categories(name: "Мужская одежда и аксессуары", icon: Icons.watch_outlined),
      Categories(name: "Мужская обувь", icon: Icons.directions_run_outlined),
      Categories(name: "Свадьба и праздники", icon: Icons.card_giftcard),
      Categories(name: "Маленькие дети и их родители", icon: Icons.child_friendly_outlined),
      Categories(name: "Одежда для детей до года (до 86 см)", icon: Icons.baby_changing_station),
      Categories(name: "Детская одежда", icon: Icons.child_care),
      Categories(name: "Детская обувь", icon: Icons.directions_walk_outlined),
      Categories(name: "Красота и здоровье", icon: Icons.medication_outlined),
      Categories(name: "Разное", icon: Icons.coffee),
    ];
  }
}

import 'TypeItem.dart';

class Item {
  final String title;
  final String description;
  final String srcUrl;
  final String price;
  final String city;
  final String link;
  final TypeItem type;
  final bool isPremium;
  final bool isTorg;
  final int id;

  Item(
      {required this.title,
        required this.description,
        required this.srcUrl,
        required this.price,
        required this.city,
        required this.link,
        required this.type,
        required this.isPremium,
        required this.isTorg,
        required this.id
      });
}

class Subcategory {
  String name;
  String link;

  Subcategory({required this.name, required this.link});
}
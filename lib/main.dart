import 'package:baraholka/core/db/FavoriteItem.dart';
import 'package:baraholka/services/favorite/FavoriteApiProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'application.dart';
import 'core/db/FavoriteItemType.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark
  ));
  SystemChrome.setEnabledSystemUIOverlays(
      [SystemUiOverlay.bottom, SystemUiOverlay.top]);
  await Hive.initFlutter();
  Hive.registerAdapter(FavoriteItemAdapter());
  Hive.registerAdapter(FavoriteItemTypeAdapter());
  await Hive.openBox<FavoriteItem>(favoriteBox);
  runApp(Application());
}
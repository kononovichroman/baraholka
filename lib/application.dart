import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/listitems/filter/ItemsFilterBloc.dart';
import 'package:baraholka/services/favorite/FavoriteRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'core/bloc/favorite/FavoriteBloc.dart';
import 'core/bloc/favorite/FavoriteEvent.dart';
import 'core/bloc/filter/FilterBloc.dart';
import 'core/bloc/filter/FilterEvent.dart';
import 'core/bloc/listitems/filter/ItemsFilterLoadEvent.dart';
import 'ext/KeyboardDismiss.dart';
import 'ext/adaptive/AdaptiveApp.dart';
import 'ext/adaptive/AdaptiveInheritance.dart';
import 'presentation/screens/HomeScreen.dart';
import 'services/filter/FilterRepo.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _prepareSVG();
    return MultiBlocProvider(
      providers: [
        BlocProvider<ItemsFilterBloc>(
            create: (context) =>
                ItemsFilterBloc(FilterRepo())..add(ItemsFilterLoadEvent())),
        BlocProvider<FilterBloc>(
            create: (context) =>
                FilterBloc(filterRepo: FilterRepo())..add(FilterLoadEvent())),
        BlocProvider<FavoriteBloc>(
            lazy: false,
            create: (context) =>
            FavoriteBloc(FavoriteRepo())..add(FavoriteLoadEvent())),
      ],
      child: AdaptiveInheritance(
          adaptiveState: AdaptiveInheritance.getStateByPlatform(),
          child: AdaptiveApp(
            home: KeyboardDismiss(child: HomeScreen()),
            materialTheme: ThemeData(primaryColor: CustomColors.kMainBlueColor),
            cupertinoTheme: CupertinoThemeData(
              scaffoldBackgroundColor: CustomColors.kMainGrayColor,
                primaryColor: CupertinoColors.white,
                brightness: Brightness.light // TODO('Temp')
                ),
          )),
    );
  }

  void _prepareSVG() {
    Future.wait([
      precachePicture(
        ExactAssetPicture(
          SvgPicture.svgStringDecoder, 'assets/svg/filter_check.svg',),
        null,
      ),
      precachePicture(
        ExactAssetPicture(
          SvgPicture.svgStringDecoder, 'assets/svg/filter.svg',),
        null,
      ),
    ]);
  }
}

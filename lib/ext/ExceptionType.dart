abstract class ExceptionType implements Exception{}

class ExceptionTypeNotFound extends ExceptionType{}
class ExceptionTypeNetwork extends ExceptionType{}
class ExceptionTypeErrorParse extends ExceptionType{}
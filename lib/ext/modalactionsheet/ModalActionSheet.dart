import 'package:baraholka/ext/adaptive/AdaptiveInheritance.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CupertinoModalActionSheet.dart';
import 'MaterialModalActionSheet.dart';
import 'SheetAction.dart';

Future<T?> showModalActionSheet<T>({
  required BuildContext context,
  String? title,
  String? message,
  List<SheetAction<T>> actions = const [],
  String? cancelLabel,
  bool isDismissible = true,
  bool useRootNavigator = true,
  MaterialModalActionSheetConfiguration? materialConfiguration,
}) {
  void pop(T? key) => Navigator.of(
        context,
        rootNavigator: useRootNavigator,
      ).pop(key);
  final AdaptiveInheritance _inheritance = AdaptiveInheritance.of(context)!;
  return _inheritance.adaptiveState == AdaptiveState.Cupertino
      ? showCupertinoModalPopup(
          context: context,
          useRootNavigator: useRootNavigator,
          builder: (context) => CupertinoModalActionSheet(
            onPressed: pop,
            title: title,
            message: message,
            actions: actions,
            cancelLabel: cancelLabel,
          ),
        )
      : showModalBottomSheet(
          context: context,
          isScrollControlled: materialConfiguration != null,
          isDismissible: isDismissible,
          useRootNavigator: useRootNavigator,
          builder: (context) => MaterialModalActionSheet(
            onPressed: pop,
            title: title,
            message: message,
            actions: actions,
            cancelLabel: cancelLabel,
            materialConfiguration: materialConfiguration,
          ),
        );
}

class MaterialModalActionSheetConfiguration {
  const MaterialModalActionSheetConfiguration({
    this.initialChildSize = 0.5,
    this.minChildSize = 0.25,
    this.maxChildSize = 0.9,
  });

  final double initialChildSize;
  final double minChildSize;
  final double maxChildSize;
}

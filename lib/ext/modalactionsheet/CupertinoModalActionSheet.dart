import 'dart:math';

import 'package:baraholka/constants/CustomColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:baraholka/ext/StringEx.dart';

import 'SheetAction.dart';

class CupertinoModalActionSheet<T> extends StatelessWidget {
  const CupertinoModalActionSheet({
    Key? key,
    required this.onPressed,
    required this.actions,
    this.title,
    this.message,
    this.cancelLabel,
  }) : super(key: key);

  final Function(T?) onPressed;
  final List<SheetAction<T>> actions;
  final String? title;
  final String? message;
  final String? cancelLabel;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final title = this.title;
    final message = this.message;
    return MediaQuery(
      data: mediaQuery.copyWith(
        // `CupertinoAlertDialog` overrides textScaleFactor
        // to keep larger than 1, but `CupertinoActionSheet` doesn't.
        // https://twitter.com/_mono/status/1266997626693509126
        textScaleFactor: max(1, mediaQuery.textScaleFactor),
      ),
      child: CupertinoActionSheet(
        title: title == null ? null : Text(title),
        message: message == null ? null : Text(message),
        cancelButton: CupertinoActionSheetAction(
          child: Text(
            cancelLabel ??
                MaterialLocalizations.of(context)
                    .cancelButtonLabel
                    .capitalizedForce,
            style: TextStyle(color: CustomColors.kMainBlueColor),
          ),
          isDefaultAction: !actions.any((a) => a.isDefaultAction),
          onPressed: () => onPressed(null),
        ),
        actions: actions
            .map((a) => CupertinoActionSheetAction(
          child: Text(a.label, style: TextStyle(color: a.isDestructiveAction ? CupertinoColors.destructiveRed : CustomColors.kMainBlueColor),),
          isDestructiveAction: a.isDestructiveAction,
          isDefaultAction: a.isDefaultAction,
          onPressed: () => onPressed(a.key),
        ))
            .toList(),
      ),
    );
  }
}
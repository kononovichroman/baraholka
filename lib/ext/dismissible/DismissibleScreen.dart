import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

const double _kDismissThreshold = 0.15;
const double _kMinScale = 0.85;

typedef DismissDirectionCallback = void Function(DismissDirection direction);

class DismissibleScreen extends StatefulWidget {
  DismissibleScreen({
    required this.child,
    this.isFullScreen = true,
    this.disabled = false,
    this.backgroundColor = Colors.black,
    this.crossAxisEndOffset = 0.0,
    this.maxTransformValue = .4,
    this.onDismiss,
    this.onDragStart,
    this.onDragEnd,
    this.reverseDuration = const Duration(milliseconds: 500),
    Key? key,
  })  : super(key: key);

  final VoidCallback? onDragStart, onDragEnd, onDismiss;
  final bool isFullScreen;
  final double maxTransformValue;
  final bool disabled;
  final Widget child;
  final Color backgroundColor;
  final double crossAxisEndOffset;
  final Duration reverseDuration;

  @override
  _DismissibleState createState() => _DismissibleState();
}

class _DismissibleState extends State<DismissibleScreen>
    with TickerProviderStateMixin {
  late AnimationController _moveController;
  late Animation<Offset> _moveAnimation;
  final DismissDirection direction = DismissDirection.down;
  double _dragExtent = 0.0;
  bool _dragUnderway = false;

  @override
  void initState() {
    super.initState();
    _moveController = AnimationController(
      duration: const Duration(microseconds: 1),
      vsync: this,
    )..addStatusListener(_handleDismissStatusChanged);
    _updateMoveAnimation();
  }

  @override
  void dispose() {
    _moveController.removeStatusListener(_handleDismissStatusChanged);
    _moveController.dispose();
    super.dispose();
  }


  bool get _isActive {
    return _dragUnderway || _moveController.isAnimating;
  }

  double get _overallDragAxisExtent {
    return context.size?.height?? 0;
  }

  void _handleDragStart(DragStartDetails details) {
    widget.onDragStart?.call();
    _dragUnderway = true;
    if (_moveController.isAnimating) {
      _dragExtent =
          _moveController.value * _overallDragAxisExtent * _dragExtent.sign;
      _moveController.stop();
    } else {
      _dragExtent = 0.0;
      _moveController.value = 0.0;
    }
    setState(() => _updateMoveAnimation());
  }

  void _handleDragUpdate(DragUpdateDetails details) {
    if (!_isActive || _moveController.isAnimating || widget.disabled) return;

    final double delta = details.primaryDelta ?? 0.0;
    final oldDragExtent = _dragExtent;
    bool _(DismissDirection d) => direction == d;

    if (_(DismissDirection.vertical)) {
      _dragExtent += delta;
    } else if (_(DismissDirection.up)) {
      if (_dragExtent + delta < 0) _dragExtent += delta;
    } else if (_(DismissDirection.down)) {
      if (_dragExtent + delta > 0) _dragExtent += delta;
    }

    if (oldDragExtent.sign != _dragExtent.sign) {
      setState(() => _updateMoveAnimation());
    }

    if (!_moveController.isAnimating) {
      _moveController.value = _dragExtent.abs() / _overallDragAxisExtent;
    }
  }

  void _updateMoveAnimation() {
    _moveAnimation = _moveController.drive(
      Tween<Offset>(
        begin: Offset.zero,
        end: Offset(widget.crossAxisEndOffset, 0.7),
      ),
    );
  }

  Future<void> _handleDragEnd(DragEndDetails details) async {
    if (!_isActive || _moveController.isAnimating) return;
    if (!_moveController.isDismissed) {
      if (_moveController.value >
          _kDismissThreshold) {
        widget.onDismiss?.call();
      } else {
        _moveController.reverseDuration = widget.reverseDuration;
        _moveController.reverse();
        widget.onDragEnd?.call();
      }
    }
  }

  void _handleDismissStatusChanged(AnimationStatus status) {
    if (status == AnimationStatus.completed && !_dragUnderway) {
      widget.onDismiss?.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    final contentPadding =
    widget.isFullScreen ? EdgeInsets.zero : MediaQuery.of(context).padding;

    if (widget.disabled) {
      return Padding(padding: contentPadding, child: widget.child);
    }

    return GestureDetector(
      onVerticalDragStart: _handleDragStart,
      onVerticalDragUpdate: _handleDragUpdate,
      onVerticalDragEnd: _handleDragEnd,
      behavior: HitTestBehavior.opaque,
      dragStartBehavior: DragStartBehavior.start,
      child: AnimatedBuilder(
        animation: _moveAnimation,
        child: widget.child,
        builder: (BuildContext context, Widget? child) {
          final k = _moveAnimation.value.dy;

          final dx = _moveAnimation.value.dx.clamp(0, widget.maxTransformValue);
          final dy = _moveAnimation.value.dy.clamp(0, widget.maxTransformValue);
          final scale = lerpDouble(1, _kMinScale, k);

          return Container(
            padding: contentPadding,
            color: widget.backgroundColor.withOpacity(1 - k),
            child: Transform(
              alignment: Alignment.center,
              transform: Matrix4.identity()
                ..translate(dx * _dragExtent, dy * _dragExtent)
                ..scale(scale, scale),
              child: child,
            ),
          );
        },
      ),
    );
  }
}
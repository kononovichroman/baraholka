import 'package:flutter/material.dart';

import 'TransparentRoute.dart';

extension DismissibleContextExt on BuildContext {
  Future pushTransparentRoute({required Widget page, RouteSettings? settings}) => Navigator.of(this).push(
    TransparentRoute(
      backgroundColor: Colors.white.withOpacity(0.0),
      builder: (_) => page,
      settings: settings
    ),
  );
}
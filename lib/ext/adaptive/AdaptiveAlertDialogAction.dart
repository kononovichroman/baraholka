import 'package:animations/animations.dart';
import 'package:baraholka/ext/modalactionsheet/ModalActionSheet.dart';
import 'package:baraholka/ext/modalactionsheet/SheetAction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:baraholka/ext/StringEx.dart';

import 'AdaptiveInheritance.dart';

class AdaptiveAlertDialogAction<T> {
  const AdaptiveAlertDialogAction({
    required this.key,
    required this.label,
    this.isDefaultAction = false,
    this.isDestructiveAction = false,
    this.textStyle = const TextStyle(),
  });

  final T key;
  final String label;

  /// Make font weight to bold(Only works for CupertinoStyle).
  final bool isDefaultAction;

  /// Make font color to destructive/error color(red).
  final bool isDestructiveAction;

  /// Change textStyle to another from default.
  ///
  /// Recommended to keep null.
  final TextStyle textStyle;
}

extension AdaptiveAlertDialogActionEx<T> on AdaptiveAlertDialogAction<T> {
  Widget convertToCupertinoDialogAction({
    required Function(T) onPressed,
  }) {
    return CupertinoDialogAction(
      child: Text(label),
      isDefaultAction: isDefaultAction,
      isDestructiveAction: isDestructiveAction,
      textStyle: textStyle,
      onPressed: () => onPressed(key),
    );
  }

  Widget convertToMaterialDialogAction({
    required Function(T) onPressed,
    required Color destructiveColor,
    required bool fullyCapitalized,
  }) {
    return TextButton(
      child: Text(
        fullyCapitalized ? label.toUpperCase() : label,
        style: textStyle.copyWith(
          color: isDestructiveAction ? destructiveColor : null,
        ),
      ),
      onPressed: () => onPressed(key),
    );
  }
}

extension AdaptiveAlertDialogActionListEx<T> on List<AdaptiveAlertDialogAction<T>> {
  List<Widget> convertToCupertinoDialogActions({
    required Function(T) onPressed,
  }) =>
      map((a) => a.convertToCupertinoDialogAction(
        onPressed: onPressed,
      )).toList();

  List<Widget> convertToMaterialDialogActions({
    required Function(T) onPressed,
    required Color destructiveColor,
    required bool fullyCapitalized,
  }) =>
      map((a) => a.convertToMaterialDialogAction(
        onPressed: onPressed,
        destructiveColor: destructiveColor,
        fullyCapitalized: fullyCapitalized,
      )).toList();

  List<SheetAction<T>> convertToSheetActions() =>
      where((a) => a.key != OkCancelResult.cancel)
          .map((a) => SheetAction(
        key: a.key,
        label: a.label,
        isDefaultAction: a.isDefaultAction,
        isDestructiveAction: a.isDestructiveAction,
      ))
          .toList();

  String? findCancelLabel() {
    try {
      return firstWhere((a) => a.key == OkCancelResult.cancel).label;
      // ignore: avoid_catching_errors
    } on StateError {
      return null;
    }
  }
}

// Result type of [showOkAlertDialog] or [showOkCancelAlertDialog].
enum OkCancelResult {
  ok,
  cancel,
}


Future<OkCancelResult> showOkCancelAlertDialog({
  required BuildContext context,
  String? title,
  String? message,
  String? okLabel,
  String? cancelLabel,
  OkCancelAlertDefaultType? defaultType,
  bool isDestructiveAction = false,
  bool barrierDismissible = true,
  bool useActionSheetForCupertino = false,
  bool useRootNavigator = true,
  VerticalDirection actionsOverflowDirection = VerticalDirection.up,
  bool fullyCapitalizedForMaterial = true,
}) async {
  final isCupertinoStyle = AdaptiveInheritance.of(context)!.adaptiveState == AdaptiveState.Cupertino;
  String defaultCancelLabel() {
    final label = MaterialLocalizations.of(context).cancelButtonLabel;
    return isCupertinoStyle ? label.capitalizedForce : label;
  }

  final result = await showAlertDialog<OkCancelResult>(
    context: context,
    title: title,
    message: message,
    barrierDismissible: barrierDismissible,
    useActionSheetForCupertino: useActionSheetForCupertino,
    useRootNavigator: useRootNavigator,
    actionsOverflowDirection: actionsOverflowDirection,
    fullyCapitalizedForMaterial: fullyCapitalizedForMaterial,
    actions: [
      AdaptiveAlertDialogAction(
        label: cancelLabel ?? defaultCancelLabel(),
        key: OkCancelResult.cancel,
        isDefaultAction: defaultType == OkCancelAlertDefaultType.cancel,
      ),
      AdaptiveAlertDialogAction(
        label: okLabel ?? MaterialLocalizations.of(context).okButtonLabel,
        key: OkCancelResult.ok,
        isDefaultAction: defaultType == OkCancelAlertDefaultType.ok,
        isDestructiveAction: isDestructiveAction,
      ),
    ],
  );
  return result ?? OkCancelResult.cancel;
}

Future<T?> showAlertDialog<T>({
  required BuildContext context,
  String? title,
  String? message,
  List<AdaptiveAlertDialogAction<T>> actions = const [],
  bool barrierDismissible = true,
  bool useActionSheetForCupertino = false,
  bool useRootNavigator = true,
  VerticalDirection actionsOverflowDirection = VerticalDirection.up,
  bool fullyCapitalizedForMaterial = true,
}) {
  void pop(T? key) => Navigator.of(
    context,
    rootNavigator: useRootNavigator,
  ).pop(key);
  final theme = Theme.of(context);
  final colorScheme = theme.colorScheme;
  final isCupertinoStyle = AdaptiveInheritance.of(context)!.adaptiveState == AdaptiveState.Cupertino;
  if (isCupertinoStyle && useActionSheetForCupertino) {
    return showModalActionSheet(
      context: context,
      title: title,
      message: message,
      cancelLabel: actions.findCancelLabel(),
      actions: actions.convertToSheetActions(),
      useRootNavigator: useRootNavigator,
    );
  }
  final titleText = title == null ? null : Text(title);
  final messageText = message == null ? null : Text(message);
  return isCupertinoStyle
      ? showCupertinoDialog(
    context: context,
    useRootNavigator: useRootNavigator,
    builder: (context) => CupertinoAlertDialog(
      title: titleText,
      content: messageText,
      actions: actions.convertToCupertinoDialogActions(
        onPressed: pop,
      ),
      // TODO(mono): Set actionsOverflowDirection if available
      // https://twitter.com/_mono/status/1261122914218160128
    ),
  )
      : showModal(
    context: context,
    useRootNavigator: useRootNavigator,
    configuration: FadeScaleTransitionConfiguration(
      barrierDismissible: barrierDismissible,
    ),
    builder: (context) => AlertDialog(
      title: titleText,
      content: messageText,
      actions: actions.convertToMaterialDialogActions(
        onPressed: pop,
        destructiveColor: colorScheme.error,
        fullyCapitalized: fullyCapitalizedForMaterial,
      ),
      actionsOverflowDirection: actionsOverflowDirection,
    ),
  );
}

// Used to specify [showOkCancelAlertDialog]'s [defaultType]
enum OkCancelAlertDefaultType {
  ok,
  cancel,
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AdaptiveInheritance.dart';

class AdaptiveActionIcon extends StatelessWidget {
  const AdaptiveActionIcon({
    Key? key,
    required this.icon,
    this.padding,
    this.color,
    this.disabledColor,
    this.alignment = Alignment.center,
    required this.onPressed,
  });

  final Widget icon;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final Color? disabledColor;
  final AlignmentGeometry alignment;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    AdaptiveInheritance _inheritance = AdaptiveInheritance.of(context)!;
    return _inheritance.adaptiveState == AdaptiveState.Material
        ? IconButton(
            key: this.key,
            icon: this.icon,
            padding: this.padding ?? const EdgeInsets.all(8.0),
            color: this.color,
            disabledColor: this.disabledColor,
            alignment: this.alignment,
            onPressed: this.onPressed,
          )
        : CupertinoButton(
            key: this.key,
            minSize: 0,
            child: this.icon,
            onPressed: this.onPressed,
            color: this.color,
            disabledColor: this.disabledColor ?? CupertinoColors.quaternarySystemFill,
            alignment: this.alignment,
            padding: EdgeInsets.zero);
  }
}

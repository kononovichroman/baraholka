import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AdaptiveInheritance.dart';

/// A button that displays either a [TextButton] or a [CupertinoButton]
/// ([ElevatedButton] or [CupertinoButton.filled] respectively) based on which platform the app
/// is currently running.
class AdaptiveButton extends StatelessWidget {
  /// The button's content
  final Widget child;

  /// The press callback
  final VoidCallback? onPressed;

  /// Internally used.
  final bool _raised;
  final bool _icon;

  /// This is used on Material only; specifies the appearance of the text in contrast to button's surface color. NOTE: no effect on Cupertino.
  final ButtonStyle? materialStyle;

  /// The callback that is called when the button is long-pressed.
  ///
  /// If this callback and [onPressed] are null, then the button will be disabled.
  ///
  /// See also:
  ///
  ///  * [enabled], which is true if the button is enabled.
  final VoidCallback? onLongPress;

  /// The fill color of the button when the button is disabled.
  ///
  /// ONLY USED ON CUPERTINO
  final Color? disabledColor;


  final Color? color;

  /// Whether the button is enabled or disabled.
  ///
  /// Buttons are disabled by default. To enable a button, set its [onPressed]
  /// or [onLongPress] properties to a non-null value.
  bool get enabled => onPressed != null || onLongPress != null;

  /// ONLY USED ON CUPERTINO
  final EdgeInsetsGeometry? padding;

  /// {@macro flutter.widgets.Clip}
  ///
  /// Defaults to [Clip.none], and must not be null.
  final Clip clipBehavior;

  /// {@macro flutter.widgets.Focus.focusNode}
  final FocusNode? focusNode;

  /// {@macro flutter.widgets.Focus.autofocus}
  final bool autofocus;

  /// ONLY USED ON CUPERTINO
  final BorderRadius? borderRadius;

  /// Used when having an icon button, currently not implemented for Cupertino.
  final Widget? icon;

  /// Used when having an icon button, currently not implemented for Cupertino.
  final Widget? label;

  AdaptiveButton({
    required this.child,
    this.onLongPress,
    this.padding,
    this.clipBehavior = Clip.none,
    this.focusNode,
    this.autofocus = false,
    required this.onPressed,
    this.materialStyle,
    this.disabledColor,
    this.color,
    this.icon,
    this.label,
  })  : _raised = false,
        _icon = false,
        borderRadius = null;

  AdaptiveButton.raised({
    Key? key,
    required this.child,
    this.onLongPress,
    this.padding,
    this.clipBehavior = Clip.none,
    this.focusNode,
    this.autofocus = false,
    required this.onPressed,
    this.materialStyle,
    this.disabledColor,
    this.color,
    this.borderRadius,
    this.icon,
    this.label,
  })  : _raised = true,
        _icon = false,
        super(key: key);

  AdaptiveButton.icon({
    Key? key,
    required this.child,
    this.onLongPress,
    this.padding,
    this.clipBehavior = Clip.none,
    this.focusNode,
    this.autofocus = false,
    required this.onPressed,
    this.materialStyle,
    this.disabledColor,
    this.color,
    this.borderRadius,
    this.icon,
    this.label,
  })  : _raised = true,
        _icon = true,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    AdaptiveInheritance _inheritance = AdaptiveInheritance.of(context)!;

    return _inheritance.adaptiveState == AdaptiveState.Material
        ? _raised
            ? _icon
                ? ElevatedButton.icon(
                    onPressed: onPressed,
                    onLongPress: onLongPress,
                    icon: icon!,
                    label: label!,
                    clipBehavior: clipBehavior,
                    focusNode: focusNode,
                    autofocus: autofocus,
                    style: materialStyle,
                    key: key,
                  )
                : ElevatedButton(
                    key: key,
                    child: child,
                    onPressed: onPressed,
                    onLongPress: onLongPress,
                    clipBehavior: clipBehavior,
                    style: materialStyle,
                    focusNode: focusNode,
                    autofocus: autofocus,
                  )
            : _icon
                ? TextButton.icon(
                    onPressed: onPressed,
                    onLongPress: onLongPress,
                    icon: icon!,
                    label: label!,
                    key: key,
                    style: materialStyle,
                    clipBehavior: clipBehavior,
                    focusNode: focusNode,
                    autofocus: autofocus,
                  )
                : TextButton(
                    child: child,
                    onPressed: onPressed,
                    onLongPress: onLongPress,
                    style: materialStyle,
                    key: key,
                    clipBehavior: clipBehavior,
                    focusNode: focusNode,
                    autofocus: autofocus,
                  )
        : _raised
            ? CupertinoButton(
                child: child,
                onPressed: onPressed,
                disabledColor:
                    disabledColor ?? CupertinoColors.quaternarySystemFill,
                padding: padding ?? const EdgeInsets.all(16.0),
                key: key,
                color: color ?? CupertinoTheme.of(context).primaryColor,
                borderRadius: borderRadius ??
                    const BorderRadius.all(Radius.circular(8.0)),
              )
            : CupertinoButton(
                child: child,
                onPressed: onPressed,
                color: color,
                disabledColor:
                    disabledColor ?? CupertinoColors.quaternarySystemFill,
                padding: padding ?? const EdgeInsets.all(16.0),
                borderRadius: borderRadius ??
                    const BorderRadius.all(Radius.circular(8.0)),
                key: key,
              );
  }
}

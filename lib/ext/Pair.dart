class Pair<E, F> {
  E _first;
  F _second;

  Pair({required first, required second})
      : _first = first,
        _second = second;

  E get first => _first;

  F get second => _second;
}

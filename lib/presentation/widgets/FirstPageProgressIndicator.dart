import 'package:flutter/material.dart';

class FirstPageProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onVerticalDragUpdate: (_) {},
        child: Center(child: CircularProgressIndicator.adaptive()));
  }
}

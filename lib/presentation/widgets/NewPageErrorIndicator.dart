import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:flutter/material.dart';

class NewPageErrorIndicator extends StatelessWidget {
  const NewPageErrorIndicator({
    Key? key,
    this.onTap,
  }) : super(key: key);
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) => SizedBox(
    height: 72,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: onTap,
          child: Text(Strings.errorNetworkWidgetRetryBtn,
              style: TextStyle(
                  fontSize: 14,
                  color: CustomColors.kMainBlueColor,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center),
        ),
        SizedBox(
          height: 12,
        ),
        Text(Strings.newPageErrorIndicatorDesc,
            style:
            TextStyle(fontSize: 10, color: CustomColors.kIconGrayColor),
            textAlign: TextAlign.center),
        SizedBox(
          height: 4,
        ),
      ],
    ),
  );
}

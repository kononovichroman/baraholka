import 'package:flutter/material.dart';

class NewPageProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 72,
      child: Container(
        margin: EdgeInsets.all(16),
        child: Center(
            child: SizedBox(
                height: 12,
                width: 12,
                child: CircularProgressIndicator.adaptive(
                  strokeWidth: 2.0,
                ))),
      ),
    );
  }
}

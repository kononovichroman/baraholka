import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:flutter/material.dart';

class ErrorNetworkWidget extends StatelessWidget {
  const ErrorNetworkWidget({
    Key? key,
    this.onTap,
  }) : super(key: key);
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragUpdate: (_) {},
      child: Container(
        decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              CustomIcons.pixel_alien,
              size: 128,
              color: Colors.black,
            ),
            SizedBox.fromSize(
              size: Size.fromHeight(16),
            ),
            Text(
              Strings.errorNetworkWidgetTitle,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            SizedBox.fromSize(
              size: Size.fromHeight(8),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Text(
                Strings.errorNetworkWidgetDesc,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox.fromSize(
              size: Size.fromHeight(16),
            ),
            ElevatedButton(
                onPressed: onTap,
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        CustomColors.kMainBlueColor)),
                child: Text(
                  Strings.errorNetworkWidgetRetryBtn,
                  style: TextStyle(color: CustomColors.kWhiteColor),
                ))
          ],
        ),
      ),
    );
  }
}

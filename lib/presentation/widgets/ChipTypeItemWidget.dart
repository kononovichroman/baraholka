import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/data/model/item/TypeItem.dart';
import 'package:flutter/material.dart';

class ChipTypeItemWidget extends StatelessWidget {
  final TypeItem type;

  ChipTypeItemWidget({required this.type});

  @override
  Widget build(BuildContext context) => Container(
    alignment: AlignmentDirectional.center,
    child: Container(
      decoration: BoxDecoration(
          color: _getColorForType().withAlpha(30),
          borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        child: Text(
          _getStringForType(),
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w600,
              color: _getColorForType()),
        ),
      ),
    ),
  );

  Color _getColorForType() {
    switch (type) {
      case TypeItem.IMPORTANT:
        return CustomColors.kChipTypeItemImportant;
      case TypeItem.BUY:
        return CustomColors.kChipTypeItemBuy;
      case TypeItem.SELL:
        return CustomColors.kChipTypeItemSell;
      case TypeItem.CLOSE:
        return CustomColors.kChipTypeItemClose;
      case TypeItem.RENT:
        return CustomColors.kChipTypeItemRent;
      case TypeItem.SERVICE:
        return CustomColors.kChipTypeItemService;
      case TypeItem.SWAP:
        return CustomColors.kChipTypeItemSwap;
    }
  }

  String _getStringForType() {
    switch (type) {
      case TypeItem.IMPORTANT:
        return Strings.chipTypeImportant;
      case TypeItem.BUY:
        return Strings.chipTypeBuy;
      case TypeItem.SELL:
        return Strings.chipTypeSell;
      case TypeItem.CLOSE:
        return Strings.chipTypeClose;
      case TypeItem.RENT:
        return Strings.chipTypeRent;
      case TypeItem.SERVICE:
        return Strings.chipTypeService;
      case TypeItem.SWAP:
        return Strings.chipTypeSwap;
    }
  }
}

import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:flutter/material.dart';

class NotFoundPageWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              CustomIcons.pixel_not_found,
              size: 128,
              color: Colors.black,
            ),
            const SizedBox(height: 24),
            const Text(
              Strings.searchScreenNotFoundTitle,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 8),
            const Text(
              Strings.searchScreenNotFoundDesc,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      ),
    );
  }

}
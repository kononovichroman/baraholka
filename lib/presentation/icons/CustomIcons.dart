import 'package:flutter/widgets.dart';

class CustomIcons {
  CustomIcons._();

  static const _kFontFam = 'CustomIcons';
  static const String? _kFontPkg = null;

  static const IconData pixel_alien = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pixel_favorite = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pixel_photo = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pixel_search = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pixel_not_found = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
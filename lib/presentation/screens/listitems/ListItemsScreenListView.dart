import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/presentation/widgets/ErrorNetworkWidget.dart';
import 'package:baraholka/presentation/widgets/FirstPageProgressIndicator.dart';
import 'package:baraholka/presentation/widgets/NewPageErrorIndicator.dart';
import 'package:baraholka/presentation/widgets/NewPageProgressIndicator.dart';
import 'package:baraholka/presentation/widgets/NotFoundPageWidget.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'ListItemsScreenItemWidget.dart';

class ListItemsScreenListView extends StatelessWidget {
  final PagingController<int, Item> pagingController;
  final Pair<List<Item>, int?>? loadedItems;

  const ListItemsScreenListView(
      {Key? key, required this.pagingController, this.loadedItems})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    loadedItems != null
        ? pagingController.appendPage(loadedItems!.first, loadedItems!.second)
        : pagingController.error = "";

    return Expanded(
        child: Scrollbar(
      child: PagedListView<int, Item>.separated(
          pagingController: pagingController,
          builderDelegate: PagedChildBuilderDelegate<Item>(
              itemBuilder: (context, item, index) =>
                  ListItemsScreenItemWidget(item: item, index: index),
              newPageProgressIndicatorBuilder: (_) =>
                  NewPageProgressIndicator(),
              newPageErrorIndicatorBuilder: (_) => NewPageErrorIndicator(
                    onTap: pagingController.retryLastFailedRequest,
                  ),
              firstPageErrorIndicatorBuilder: (_) => ErrorNetworkWidget(
                    onTap: pagingController.refresh,
                  ),
              noItemsFoundIndicatorBuilder: (_) => NotFoundPageWidget(),
              firstPageProgressIndicatorBuilder: (_) =>
                  FirstPageProgressIndicator()),
          separatorBuilder: (context, index) => Container()),
    ));
  }
}

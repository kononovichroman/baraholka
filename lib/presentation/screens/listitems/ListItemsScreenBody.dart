import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/listitems/ItemsBloc.dart';
import 'package:baraholka/core/bloc/listitems/ItemsState.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreenListView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class ListItemsScreenBody extends StatelessWidget{
  const ListItemsScreenBody({required this.pagingController});

  final PagingController<int, Item> pagingController;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ItemsBloc, ItemsState>(
      builder: (context, state) {
        if (state is ItemsLoadedState) {
          return Container(
            decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
            child: Column(
              children: [
                ListItemsScreenListView(pagingController: pagingController, loadedItems: state.loadedItems),
              ],
            ),
          );
        }
        if (state is ItemsErrorState) {
          return Container(
            decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
            child: Column(
              children: [
                ListItemsScreenListView(pagingController: pagingController, loadedItems: null),
              ],
            ),
          );
        }
        if (state is ItemsLoadingState) {
          return Center(child: CircularProgressIndicator.adaptive());
        }
        return Container();
      },
    );
  }

}
import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/listitems/ItemsBloc.dart';
import 'package:baraholka/core/bloc/listitems/ItemsEvent.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreenFilterBtn.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'ListItemsScreenBody.dart';

class ListItemsScreen extends StatefulWidget {
  static var routeName = '/listitems';
  final ItemsRepo itemsRepo;
  final FilterRepo filterRepo;

  const ListItemsScreen({required this.itemsRepo, required this.filterRepo});

  @override
  _ListItemsScreenState createState() => _ListItemsScreenState();
}

class _ListItemsScreenState extends State<ListItemsScreen> {
  late ItemsBloc itemsBloc;
  int _idSubCategory = 0;
  final PagingController<int, Item> _pagingController = PagingController(
    firstPageKey: 1,
  );

  @override
  Widget build(BuildContext context) {
    RouteSettings? settings = ModalRoute.of(context)?.settings;
    Pair<String, int> pair = settings?.arguments as Pair<String, int>;
    _idSubCategory = pair.second;

    return BlocProvider<ItemsBloc>(
      create: (context) =>
          itemsBloc..add(ItemsLoadEvent(idSubCategory: pair.second)),
      child: AdaptiveScaffold(
        cupertinoNavigationBar: CupertinoNavigationBar(
          middle: Text(pair.first,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: CustomColors.kWhiteColor)),
          automaticallyImplyLeading: true,
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
          trailing: ListItemsScreenFilterBtn(
            parentScreenIsListItems: true,
            returnData: () {
              _pagingController.refresh();
            },
          ),
        ),
        appBar: AppBar(
          title: Text(pair.first,
              style: TextStyle(color: CustomColors.kWhiteColor)),
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
          actions: [
            ListItemsScreenFilterBtn(
              parentScreenIsListItems: true,
              returnData: () {
                _pagingController.refresh();
              },
            ),
          ],
        ),
        body: ListItemsScreenBody(pagingController: _pagingController),
      ),
    );
  }

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      itemsBloc.add(
          ItemsLoadEvent(idSubCategory: _idSubCategory, nextPage: pageKey));
    });
    itemsBloc = ItemsBloc(widget.itemsRepo, widget.filterRepo);
    super.initState();
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

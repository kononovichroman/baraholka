import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreen.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreenCarouselNoImage.dart';
import 'package:baraholka/presentation/widgets/ChipTypeItemWidget.dart';
import 'package:baraholka/services/favorite/FavoriteRepo.dart';
import 'package:baraholka/services/fullitem/FullItemRepo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListItemsScreenItemWidget extends StatelessWidget {
  static const double _sizeImage = 60;
  static const double _marginItem = 8;

  static const double _paddingItemVertical = 8;
  static const double _paddingItemHorizontal = 16;

  final Item item;
  final int index;

  ListItemsScreenItemWidget({required this.item, required this.index});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
            builder: (context) => FullItemScreen(fullItemRepo: FullItemRepo(), favoriteRepo: FavoriteRepo(),),
            settings: RouteSettings(
                name: FullItemScreen.routeName, arguments: item)));
      },
      child: Container(
        margin: EdgeInsets.all(_marginItem),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                  vertical: _paddingItemVertical,
                  horizontal: _paddingItemHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width -
                            _calcAuxiliaryWidth()), //124
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          item.title,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w800,
                              color: Colors.black),
                          maxLines: 2,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          item.description,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                          maxLines: 3,
                        ),
                      ],
                    ),
                  ),
                  ClipRRect(
                    child: CachedNetworkImage(
                      imageUrl: item.srcUrl,
                      placeholder: (context, url) => Center(
                          child: SizedBox(
                              height: 12,
                              width: 12,
                              child: CircularProgressIndicator.adaptive(
                                strokeWidth: 2.0,
                              ))),
                      errorWidget: (context, url, error){
                        return Container(
                          alignment: Alignment.center,
                          color: CustomColors.kMainGrayColor,
                          child: Icon(CustomIcons.pixel_photo, size: 24, color: Colors.black,),
                        );
                      },
                      fit: BoxFit.cover,
                      height: _sizeImage,
                      width: _sizeImage,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  )
                ],
              ),
            ),
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Text(
                            item.price,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                          ),
                        ),
                        item.isTorg
                            ? Text(
                                Strings.textTorg,
                                style:
                                    TextStyle(fontSize: 10, color: Colors.red),
                              )
                            : Container(),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: Text(
                        item.city,
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                    ),
                  ],
                ),
                ChipTypeItemWidget(type: item.type),
              ],
            ),
            SizedBox(
              height: 6,
            ),
            item.isPremium
                ? Container(
                    height: 6,
                    decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8))),
                  )
                : Container(height: 4),
          ],
        ),
      ),
    );
  }

  double _calcAuxiliaryWidth() =>
      _marginItem * 2 + _paddingItemHorizontal * 3 + _sizeImage;
}

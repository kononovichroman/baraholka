import 'package:baraholka/core/bloc/listitems/filter/ItemsFilterBloc.dart';
import 'package:baraholka/core/bloc/listitems/filter/ItemsFilterLoadEvent.dart';
import 'package:baraholka/core/bloc/listitems/filter/ItemsFilterState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveActionIcon.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:baraholka/presentation/screens/filter/FilterScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'ListItemsScreen.dart';

class ListItemsScreenFilterBtn extends StatelessWidget {
  const ListItemsScreenFilterBtn(
      {Key? key, this.returnData, this.parentScreenIsListItems})
      : super(key: key);
  final VoidCallback? returnData;
  final bool? parentScreenIsListItems;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ItemsFilterBloc, ItemsFilterState>(
        builder: (context, state) {
      if (state is ItemsFilterNonEmptyState) {
        return AdaptiveActionIcon(
            icon: SvgPicture.asset(
              'assets/svg/filter_check.svg',
              height: 24,
              width: 24,
            ),
            onPressed: () {
              // _test(context);
              _navigateAndCheckRefreshData(context, parentScreenIsListItems);
            });
      }
      return AdaptiveActionIcon(
        icon: SvgPicture.asset(
          'assets/svg/filter.svg',
          height: 24,
          width: 24,
        ),
        onPressed: () {
          // _test(context);
          _navigateAndCheckRefreshData(context, parentScreenIsListItems);
        },
      );
    });
  }

  void _test(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        useRootNavigator: true,
        context: context,
        duration: const Duration(milliseconds: 300),
        builder: (context) => const FilterScreen());
  }

  void _navigateAndCheckRefreshData(
      BuildContext context, bool? parentScreenIsListItems) async {
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FilterScreen(),
        settings: RouteSettings(
            name: FilterScreen.routeName,
            arguments: parentScreenIsListItems ?? false
                ? ListItemsScreen.routeName
                : null)));
    BlocProvider.of<ItemsFilterBloc>(context).add(ItemsFilterLoadEvent());
    if (result != null) {
      returnData?.call();
    }
  }
}

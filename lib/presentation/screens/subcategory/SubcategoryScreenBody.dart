import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryBloc.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryEvent.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryState.dart';
import 'package:baraholka/presentation/widgets/ErrorNetworkWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SubcategoryScreenListItems.dart';

class SubcategoryScreenBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SubcategoryBloc, SubcategoryState>(
        builder: (context, state) {
      if (state is SubcategoryLoadedState) {
        return Container(
          decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
          child: SubcategoryScreenListItems(
              listSubcategory: state.loadedSubcategory),
        );
      }
      if (state is SubcategoryLoadingState) {
        return Center(child: CircularProgressIndicator.adaptive());
      }
      if (state is SubcategoryErrorState) {
        return ErrorNetworkWidget(
          onTap: () => BlocProvider.of<SubcategoryBloc>(context).add(
              SubcategoryLoadEvent(
                  idCategory: BlocProvider.of<SubcategoryBloc>(context).lastIdCategory)),
        );
      }
      return Container();
    });
  }
}

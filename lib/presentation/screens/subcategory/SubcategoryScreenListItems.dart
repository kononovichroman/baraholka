import 'dart:io';

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryEvent.dart';
import 'package:baraholka/data/model/Subcategory.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreen.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SubcategoryScreenListItems extends StatelessWidget {
  final List<Subcategory> listSubcategory;

  const SubcategoryScreenListItems({Key? key, required this.listSubcategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
      child: Column(
        children: [
          Expanded(
            child: ListView.separated(
              itemCount: listSubcategory.length,
              itemBuilder: (context, index) {
                return Material(
                  child: ListTile(
                    onTap: () {
                      Pair<String, int> pair = Pair<String, int>(
                          first: listSubcategory[index].name,
                          second: SubcategoryClickEvent(item: listSubcategory[index]).idSubcategory);

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListItemsScreen(itemsRepo: ItemsRepo(), filterRepo: FilterRepo()),
                              settings: RouteSettings(
                                  name: ListItemsScreen.routeName, arguments: pair)));
                    },
                    title: Text(
                      listSubcategory[index].name,
                    ),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24),
                    tileColor: CustomColors.kWhiteColor,
                    trailing: Platform.isIOS
                        ? Icon(CupertinoIcons.right_chevron)
                        : null,
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Container(
                    decoration: BoxDecoration(color: CustomColors.kWhiteColor),
                    child: Divider(
                      height: 1,
                    ));
              },
            ),
          )
        ],
      ),
    );
  }
}

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryBloc.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/subcategory/SubcategoryScreenBody.dart';
import 'package:baraholka/services/subcategory/SubcategoryRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:baraholka/core/bloc/subcategory/SubcategoryEvent.dart';

class SubcategoryScreen extends StatelessWidget {
  static var routeName = '/subcategory';
  final subcategoryRepo = SubcategoryRepo();

  @override
  Widget build(BuildContext context) {
    RouteSettings? settings = ModalRoute.of(context)?.settings;
    Pair<String, int> pair = settings?.arguments as Pair<String, int>;
    int _idSubcategory = pair.second;
    SubcategoryBloc _subcategoryBloc = SubcategoryBloc(subcategoryRepo);

    return BlocProvider<SubcategoryBloc>(
      create: (context) => _subcategoryBloc
        ..add(SubcategoryLoadEvent(idCategory: _idSubcategory)),
      child: AdaptiveScaffold(
        cupertinoNavigationBar: CupertinoNavigationBar(
          middle: Text(pair.first,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: CustomColors.kWhiteColor)),
          automaticallyImplyLeading: true,
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
        ),
        appBar: AppBar(
          title: Text(pair.first,
              style: TextStyle(color: CustomColors.kWhiteColor)),
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
        ),
        body: SubcategoryScreenBody(),
      ),
    );
  }
}

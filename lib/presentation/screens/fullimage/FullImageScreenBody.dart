import 'package:baraholka/ext/dismissible/DismissibleScreen.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreenBody.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreenCarouselNoImage.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/shims/dart_ui.dart';

class FullImageScreenBody extends StatelessWidget {
  final List<String> imgList;
  final Function(int)? onPageChanged;
  final VoidCallback? onDismissListener;
  final int initPage;

  FullImageScreenBody(
      {Key? key,
      required this.imgList,
      this.onPageChanged,
      this.initPage = 0,
      this.onDismissListener})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DismissibleScreen(
      onDismiss: onDismissListener,
      backgroundColor: Colors.white,
      child: Center(
        child: CarouselSlider(
          items: imgList
              .map((item) => Hero(
                    tag: item,
                    child: Image.network(
                      item,
                      fit: BoxFit.scaleDown,
                      loadingBuilder: (context, child, loadingProgress) =>
                          loadingProgress == null
                              ? child
                              : CircularProgressIndicator.adaptive(),
                      errorBuilder: (context, obj, stackTrace) => Center(
                          child: SizedBox(
                              width: double.infinity,
                              child: const FullItemScreenCarouselNoImage())),
                    ),
                  ))
              .toList(),
          options: CarouselOptions(
            viewportFraction: 1.0,
            autoPlay: false,
            initialPage: initPage,
            height: double.infinity,
            onPageChanged: (index, reason) {
              onPageChanged?.call(index);
            },
            enableInfiniteScroll: imgList.length > 1,
            enlargeCenterPage: false,
          ),
        ),
      ),
    );
  }
}

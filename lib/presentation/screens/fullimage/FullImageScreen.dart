import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/fullimage/FullImageScreenTitleText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'FullImageScreenBody.dart';

class FullImageScreen extends StatefulWidget {
  static var routeName = '/fullimage';

  const FullImageScreen({Key? key}) : super(key: key);

  @override
  _FullImageScreenState createState() => _FullImageScreenState();
}

class _FullImageScreenState extends State<FullImageScreen> {
  int? _currentPage;

  @override
  Widget build(BuildContext context) {
    RouteSettings? settings = ModalRoute.of(context)?.settings;
    final Pair<List<String>, int> pair =
        settings?.arguments as Pair<List<String>, int>;
    _currentPage = _currentPage ?? pair.second;
    return AdaptiveScaffold(
      cupertinoNavigationBar: CupertinoNavigationBar(
        leading: CupertinoButton(
          child: const Text(Strings.fullImageScreenCloseBtnCupertino),
          padding: EdgeInsets.zero,
          onPressed: () {
            _popScreen();
          },
        ),
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        middle: FullImageScreenTitleText(
          currentPage: (_currentPage ?? 0) + 1,
          countPages: pair.first.length,
        ),
      ),
      appBar: AppBar(
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        title: FullImageScreenTitleText(
          currentPage: (_currentPage ?? 0) + 1,
          countPages: pair.first.length,
        ),
      ),
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () async {
          _popScreen();
          return true;
        },
        child: FullImageScreenBody(
          imgList: pair.first,
          onPageChanged: (page) {
            setState(() {
              _currentPage = page;
            });
          },
          onDismissListener: () {
            _popScreen();
          },
          initPage: pair.second,
        ),
      ),
    );
  }

  void _popScreen() {
    Navigator.pop(context, _currentPage);
  }
}

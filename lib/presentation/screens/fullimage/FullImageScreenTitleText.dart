import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:flutter/material.dart';

class FullImageScreenTitleText extends StatelessWidget {
  const FullImageScreenTitleText(
      {Key? key, required this.currentPage, required this.countPages})
      : super(key: key);
  final int currentPage;
  final int countPages;

  @override
  Widget build(BuildContext context) {
    return Text(
        currentPage.toString() +
            Strings.fullImageScreenAppBarTitle +
            countPages.toString(),
        style: TextStyle(color: CustomColors.kWhiteColor));
  }
}

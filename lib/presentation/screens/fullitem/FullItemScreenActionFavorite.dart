import 'dart:io';

import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteEvent.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemBloc.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemEvent.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveActionIcon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FullItemScreenActionFavorite extends StatelessWidget {
  const FullItemScreenActionFavorite({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FullItemBloc, FullItemState>(builder: (context, state) {
      return AdaptiveActionIcon(
          icon: Icon(state is FullItemLoadedState && state.isFavorite
              ? Platform.isIOS
                  ? CupertinoIcons.heart_fill
                  : Icons.favorite
              : Platform.isIOS
                  ? CupertinoIcons.heart
                  : Icons.favorite_border),
          onPressed: state is FullItemLoadedState ? () {
            BlocProvider.of<FullItemBloc>(context).add(FullItemClickFavoriteAction());
            BlocProvider.of<FavoriteBloc>(context).add(FavoriteLoadEvent());
          } : null);
    });
  }
}

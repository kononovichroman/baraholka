import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class FullItemScreenDesc extends StatelessWidget{
  final String description;

  const FullItemScreenDesc({Key? key, required this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Html(
          data: description,
          onLinkTap: (url, _, __, ___) {
            if (url != null) {
              launch(url);
            }
          }),
    );
  }

}
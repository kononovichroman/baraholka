import 'package:baraholka/constants/Strings.dart';
import 'package:flutter/material.dart';

class FullItemScreenPrice extends StatelessWidget {
  final String? price;
  final bool? isTorg;

  const FullItemScreenPrice({Key? key, this.price, this.isTorg})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 8),
          child: Text(
            price ?? Strings.textPriceNotSpecified,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800),
          ),
        ),
        if (isTorg ?? false)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 0),
            child: Text(
              Strings.textTorg,
              style: TextStyle(
                  fontSize: 14, fontWeight: FontWeight.w400, color: Colors.red),
            ),
          )
        else
          const SizedBox(),
      ],
    );
  }
}

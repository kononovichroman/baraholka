import 'dart:ui';

import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/presentation/screens/fullimage/FullImageScreen.dart';
import 'package:baraholka/presentation/screens/fullitem/carousel/FullItemScreenCarouselIndicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:baraholka/ext/dismissible/DismissibleContextExt.dart';
import '../FullItemScreenBody.dart';
import '../FullItemScreenCarouselNoImage.dart';

class FullItemScreenCarouselImage extends StatefulWidget {
  final List<String> imgList;
  final ScrollController scrollController;
  FullItemScreenCarouselImage({Key? key, required this.imgList, required this.scrollController});

  @override
  _FullItemScreenCarouselImageState createState() =>
      _FullItemScreenCarouselImageState();
}

class _FullItemScreenCarouselImageState
    extends State<FullItemScreenCarouselImage> {
  int _current = 0;
  late String _bgImage;
  late CarouselController _carouselController;

  @override
  void initState() {
    _bgImage = widget.imgList.first;
    _carouselController = CarouselController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final result = await context.pushTransparentRoute(
            page: FullImageScreen(),
            settings: RouteSettings(
                name: FullImageScreen.routeName,
                arguments: Pair<List<String>, int>(
                    first: widget.imgList, second: _current)));

        if (result != null) {
          widget.scrollController.animateTo(0.0, duration: Duration(microseconds: 1), curve: Curves.linear);
          _carouselController.jumpToPage(result as int);
        }
      },
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Image.network(_bgImage,
              fit: BoxFit.cover,
              width: double.infinity,
              height: kHeightFullItemImage,
              errorBuilder: (context, obj, stackTrace) => Container()),
          ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
              child: CarouselSlider(
                carouselController: _carouselController,
                items: widget.imgList
                    .map((item) => Hero(
                        tag: item,
                        child: Image.network(
                          item,
                          fit: BoxFit.fitHeight,
                          loadingBuilder: (context, child, loadingProgress) =>
                              loadingProgress == null
                                  ? child
                                  : CircularProgressIndicator.adaptive(),
                          errorBuilder: (context, obj, stackTrace) => SizedBox(
                              width: double.infinity,
                              child: const FullItemScreenCarouselNoImage()),
                        )))
                    .toList(),
                options: CarouselOptions(
                    viewportFraction: 1.0,
                    autoPlay: false,
                    enableInfiniteScroll: widget.imgList.length > 1,
                    enlargeCenterPage: false,
                    height: kHeightFullItemImage,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                        _bgImage = widget.imgList[index];
                      });
                    }),
              ),
            ),
          ),
          FullItemScreenCarouselIndicator(
              currentImage: _current + 1, countImage: widget.imgList.length)
        ],
      ),
    );
  }
}

import 'package:baraholka/constants/Strings.dart';
import 'package:flutter/material.dart';

class FullItemScreenCarouselIndicator extends StatelessWidget {
  final int currentImage;
  final int countImage;

  const FullItemScreenCarouselIndicator(
      {Key? key, required this.currentImage, required this.countImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.black.withAlpha(170),
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
          child: Text(
            currentImage.toString() +
                Strings.fullItemScreenCarouselIndicator +
                countImage.toString(),
            style: TextStyle(color: Colors.white, fontSize: 11),
          ),
        ),
      ),
    );
  }
}

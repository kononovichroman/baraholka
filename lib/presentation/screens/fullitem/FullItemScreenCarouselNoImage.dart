import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:flutter/material.dart';

import 'FullItemScreenBody.dart';

class FullItemScreenCarouselNoImage extends StatelessWidget{
  const FullItemScreenCarouselNoImage({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: kHeightFullItemImage,
        child: Container(
          color: CustomColors.kMainGrayColor,
          child: Icon(CustomIcons.pixel_photo, size: 80, color: Colors.black,),
        ));
  }

}
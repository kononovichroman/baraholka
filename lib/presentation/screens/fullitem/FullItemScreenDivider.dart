import 'package:baraholka/constants/CustomColors.dart';
import 'package:flutter/material.dart';

class FullItemScreenDivider extends StatelessWidget {
  const FullItemScreenDivider({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Container(
        height: 16,
        decoration: const BoxDecoration(color: CustomColors.kMainGrayColor),
      ),
    );
  }
}

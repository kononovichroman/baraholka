import 'package:flutter/material.dart';

class FullItemScreenUpAndCity extends StatelessWidget {
  final int countUp;
  final String afterUp;
  final String city;

  const FullItemScreenUpAndCity(
      {Key? key,
      required this.countUp,
      required this.afterUp,
      required this.city})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              countUp == 0
                  ? const SizedBox()
                  : Container(
                      height: 36,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.orangeAccent.withAlpha(50)),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 4, top: 8, bottom: 8, right: 12),
                        child: Row(
                          children: [
                            Icon(
                              Icons.arrow_drop_up,
                              color: Colors.orange,
                            ),
                            Text(
                              countUp.toString(),
                              style: TextStyle(
                                color: Colors.orange,
                              ),
                            ),
                          ],
                        ),
                      ),
                      alignment: Alignment.center,
                    ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  afterUp,
                  style: TextStyle(fontSize: 12, color: Colors.grey),
                ),
              ),
            ],
          ),
          Text(
            city,
            style: TextStyle(fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemBloc.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemEvent.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/services/favorite/FavoriteRepoAPI.dart';
import 'package:baraholka/services/fullitem/FullItemRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FullItemScreenActionFavorite.dart';
import 'FullItemScreenActionShare.dart';
import 'FullItemScreenBody.dart';
import 'FullItemScreenCallBtn.dart';

class FullItemScreen extends StatelessWidget {
  final FullItemRepo fullItemRepo;
  final FavoriteRepoAPI favoriteRepo;
  static var routeName = '/fullItem';

  const FullItemScreen({required this.fullItemRepo, required this.favoriteRepo});

  @override
  Widget build(BuildContext context) {
    RouteSettings? settings = ModalRoute.of(context)?.settings;

    Item item = settings?.arguments as Item;
    return BlocProvider<FullItemBloc>(
      create: (context) => FullItemBloc(fullItemRepo, favoriteRepo)
        ..add(FullItemLoadEvent(item: item)),
      child: AdaptiveScaffold(
        cupertinoNavigationBar: CupertinoNavigationBar(
          automaticallyImplyLeading: true,
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const FullItemScreenActionShare(),
              const SizedBox(
                width: 24,
              ),
              const FullItemScreenActionFavorite()
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: CustomColors.kMainBlueColor,
          brightness: Brightness.dark,
          actions: [
            const FullItemScreenActionShare(),
            const FullItemScreenActionFavorite()
          ],
        ),
        backgroundColor: CustomColors.kWhiteColor,
        body: Stack(children: [
          FullItemScreenBody(
            titleText: item.title,
          ),
          SafeArea(
              child: Container(
            child: const FullItemScreenCallBtn(),
            alignment: Alignment.bottomCenter,
          ))
        ]),
      ),
    );
  }
}

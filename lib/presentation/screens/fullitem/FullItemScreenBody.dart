import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteEvent.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemBloc.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemEvent.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemState.dart';
import 'package:baraholka/data/model/fullitem/FullItem.dart';
import 'package:baraholka/ext/ExceptionType.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreenDesc.dart';
import 'package:baraholka/presentation/screens/fullitem/FullItemScreenUserBlock.dart';
import 'package:baraholka/presentation/screens/fullitem/carousel/FullItemScreenCarouselImage.dart';
import 'package:baraholka/presentation/widgets/ErrorNetworkWidget.dart';
import 'package:baraholka/presentation/widgets/PageClosedWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FullItemScreenCarouselNoImage.dart';
import 'FullItemScreenDivider.dart';
import 'loading/FullItemScreenLoading.dart';
import 'FullItemScreenPrice.dart';
import 'FullItemScreenTitle.dart';
import 'FullItemScreenUpAndCity.dart';

const kHeightFullItemImage = 360.0;

class FullItemScreenBody extends StatefulWidget {
  FullItemScreenBody({Key? key, required this.titleText}) : super(key: key);
  final String titleText;

  @override
  _FullItemScreenBodyState createState() => _FullItemScreenBodyState();
}

class _FullItemScreenBodyState extends State<FullItemScreenBody> {
  late ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FullItemBloc, FullItemState>(builder: (context, state) {
      if (state is FullItemLoadedState) {
        FullItem _fullItem = state.fullItem;
        return Scrollbar(
          child: ListView(
            controller: _scrollController,
            children: [
              state.fullItem.srcUrls.isNotEmpty
                  ? FullItemScreenCarouselImage(
                      scrollController: _scrollController,
                      imgList: state.fullItem.srcUrls)
                  : const FullItemScreenCarouselNoImage(),
              FullItemScreenTitle(title: _fullItem.title),
              FullItemScreenPrice(
                  price: _fullItem.price, isTorg: _fullItem.isTorg),
              FullItemScreenUpAndCity(
                countUp: _fullItem.countUp,
                afterUp: _fullItem.afterUp,
                city: _fullItem.city,
              ),
              const FullItemScreenDivider(),
              FullItemScreenDesc(description: _fullItem.description),
              const FullItemScreenDivider(),
              FullItemScreenUserBlock(
                urlAvatar: _fullItem.avatarAuthor,
                name: _fullItem.nameAuthor,
                linkWriteSeller: _fullItem.linkWriteAuthor,
                allAdsSeller: _fullItem.allAdsAuthor,
              ),
              const SizedBox(height: 64)
            ],
          ),
        );
      }
      if (state is FullItemErrorState) {
        if (state.error is ExceptionTypeNotFound) {
          BlocProvider.of<FavoriteBloc>(context).add(FavoriteLoadEvent());
          return const PageClosedWidget();
        } else if (state.error is ExceptionTypeNetwork) {
          return ErrorNetworkWidget(
            onTap: () {
              BlocProvider.of<FullItemBloc>(context)
                  .add(FullItemRetryLoadEvent());
            },
          );
        } else {
          return Text(state.error.toString());
        }
      }

      if (state is FullItemLoadingState) {
        return FullItemScreenLoading(text: widget.titleText);
      }
      return const SizedBox();
    });
  }
}

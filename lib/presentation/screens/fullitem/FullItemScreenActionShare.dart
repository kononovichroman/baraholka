import 'dart:ffi';
import 'dart:io';

import 'package:baraholka/core/bloc/fullitem/FullItemBloc.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveActionIcon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';

class FullItemScreenActionShare extends StatelessWidget {

  const FullItemScreenActionShare({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FullItemBloc, FullItemState>(builder: (context, state) {
      return AdaptiveActionIcon(
          icon: Icon(Platform.isIOS ? CupertinoIcons.share : Icons.share),
          onPressed: state is FullItemLoadedState ? () {
            Share.share(state.fullItem.link);
          } : null);
    });
  }
}

import 'dart:io';

import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemBloc.dart';
import 'package:baraholka/core/bloc/fullitem/FullItemState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveButton.dart';
import 'package:baraholka/ext/modalactionsheet/ModalActionSheet.dart';
import 'package:baraholka/ext/modalactionsheet/SheetAction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class FullItemScreenCallBtn extends StatelessWidget {
  const FullItemScreenCallBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FullItemBloc, FullItemState>(builder: (context, state) {
      if (state is FullItemLoadedState &&
          state.fullItem.numbersPhone.isNotEmpty) {
        return SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: AdaptiveButton.raised(
                child: Platform.isIOS
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(CupertinoIcons.phone_fill),
                          SizedBox(
                            width: 16,
                          ),
                          Text(Strings.fullItemScreenCallBtn),
                        ],
                      )
                    : Text(Strings.fullItemScreenCallBtn),
                color: Colors.lightGreen,
                onPressed: () {
                  _showPhones(context, state.fullItem.numbersPhone);
                }),
          ),
        );
      }
      return const SizedBox();
    });
  }

  void _showPhones(BuildContext context, List<String> numbersPhone) async {
    final result = await showModalActionSheet<String>(
        cancelLabel: Strings.fullItemScreenModalActionCancel,
        title: Strings.fullItemScreenModalActionTitle,
        context: context,
        actions: numbersPhone
            .map((e) => SheetAction(label: e, key: e, icon: Icons.phone))
            .toList());
    if(result != null) {
      launch('tel:$result');
    }
  }
}

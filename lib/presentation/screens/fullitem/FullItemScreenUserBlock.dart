import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class FullItemScreenUserBlock extends StatelessWidget {
  final String urlAvatar;
  final String name;
  final String linkWriteSeller;
  final String allAdsSeller;

  const FullItemScreenUserBlock(
      {Key? key,
      required this.urlAvatar,
      required this.name,
      required this.linkWriteSeller,
      required this.allAdsSeller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 24,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: 64,
                width: 64,
                child: CircleAvatar(
                  child: ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: urlAvatar,
                      placeholder: (context, url) => Center(
                          child: const SizedBox(
                              height: 12,
                              width: 12,
                              child: CircularProgressIndicator.adaptive(
                                strokeWidth: 2.0,
                              ))),
                      fit: BoxFit.fill,
                    ),
                  ),
                )),
            const SizedBox(
              width: 16,
            ),
            Text(
              name,
              style: TextStyle(fontWeight: FontWeight.w600),
            )
          ],
        ),
        const SizedBox(
          height: 24,
        ),
        GestureDetector(
          onTap: () {
            launch(allAdsSeller);
          },
          child: Text(
            Strings.fullItemScreenAllAds,
            style: TextStyle(
                color: CustomColors.kMainBlueColor,
                fontWeight: FontWeight.w600),
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        GestureDetector(
          onTap: () {
            launch(linkWriteSeller);
          },
          child: Text(
            Strings.fullItemScreenWriteSeller,
            style: TextStyle(
                color: CustomColors.kMainBlueColor,
                fontWeight: FontWeight.w600),
          ),
        ),
        const SizedBox(
          height: 24,
        ),
      ],
    );
  }
}

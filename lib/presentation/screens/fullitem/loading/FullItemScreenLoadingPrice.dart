import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FullItemScreenLoadingPrice extends StatelessWidget{
  const FullItemScreenLoadingPrice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double rightPadding = MediaQuery.of(context).size.width * 2 / 3;
    return Shimmer.fromColors(
        baseColor: Colors.grey.withAlpha(50),
        highlightColor: Colors.white,
        child: Padding(
          padding:
          EdgeInsets.only(left: 16, top: 16, bottom: 8, right: rightPadding),
          child: Container(
            color: Colors.blueGrey,
            height: 16,
            width: MediaQuery.of(context).size.width / 3,
          ),
        ));
  }

}
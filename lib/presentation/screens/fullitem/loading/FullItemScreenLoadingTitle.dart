import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FullItemScreenLoadingTitle extends StatelessWidget{
  const FullItemScreenLoadingTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.grey.withAlpha(50),
        highlightColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(top: 3.0, left: 16.0,right: 16.0, bottom: 3.0),
          child: Container(
            color: Colors.blueGrey,
            height: 20,
          ),
        ));
  }

}
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FullItemScreenLoadingImage extends StatelessWidget {
  const FullItemScreenLoadingImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.grey.withAlpha(50),
        highlightColor: Colors.white,
        child: SizedBox(height: 360, child: Container(color: Colors.blueGrey)));
  }
}

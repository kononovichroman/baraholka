import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FullItemScreenLoadingUpAndCity extends StatelessWidget{
  const FullItemScreenLoadingUpAndCity({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.grey.withAlpha(50),
        highlightColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 36,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(16),
                        color: Colors.orangeAccent),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 4, top: 8, bottom: 8, right: 12),
                      child: Container(
                        width: 48,
                      ),
                    ),
                    alignment: Alignment.center,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(height: 12, width: 80,color: Colors.blueGrey),
                  ),
                ],
              ),
              Container(height: 14, width: 96, color: Colors.blueGrey)
            ],
          ),
        ));
  }

}
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FullItemScreenLoadingDesc extends StatelessWidget{
  const FullItemScreenLoadingDesc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.grey.withAlpha(50),
        highlightColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Container(
            color: Colors.blueGrey,
            height: 24,
          ),
        ));
  }

}
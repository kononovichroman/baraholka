import 'dart:ui';

import 'package:flutter/material.dart';

import '../FullItemScreenDivider.dart';
import 'FullItemScreenLoadingDesc.dart';
import 'FullItemScreenLoadingImage.dart';
import 'FullItemScreenLoadingPrice.dart';
import 'FullItemScreenLoadingTitle.dart';
import 'FullItemScreenLoadingUpAndCity.dart';

class FullItemScreenLoading extends StatelessWidget {
  const FullItemScreenLoading({Key? key, required this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      children: [
        const FullItemScreenLoadingImage(),
        const SizedBox(height: 13,),
        LayoutBuilder(builder: (context, size) {
          final span = TextSpan(
              text: text,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600));
          final tp = TextPainter(text: span, textDirection: TextDirection.ltr);
          tp.layout(maxWidth: size.maxWidth);
          return Column(
            children: tp
                .computeLineMetrics()
                .map((e) => const FullItemScreenLoadingTitle())
                .toList(),
          );
        }),
        const FullItemScreenLoadingPrice(),
        const FullItemScreenLoadingUpAndCity(),
        const FullItemScreenDivider(),
        const FullItemScreenLoadingDesc(),
        const FullItemScreenLoadingDesc(),
        const FullItemScreenLoadingDesc(),
        const FullItemScreenLoadingDesc(),
      ],
    );
  }
}

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterState.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/presentation/screens/filter/FilterScreenRadioBtnsCategory.dart';
import 'package:baraholka/presentation/screens/filter/FilterScreenRadioBtnsRegion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FilterScreenDivider.dart';
import 'FilterScreenOnlyTitleSection.dart';
import 'FilterScreenRadioBtnsSort.dart';
import 'FilterScreenTitleSection.dart';

class FilterScreenBody extends StatelessWidget {
  final bool parentScreenIsListItems;

  const FilterScreenBody(this.parentScreenIsListItems);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(builder: (context, state) {
      if (state is FilterLoadedState) {
        return Container(
          decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
          child: ListView(
            children: [
              !parentScreenIsListItems
                  ? const FilterScreenTitleSection(
                      text: Strings.filterScreenFilterSectionTitle)
                  : Container(),
              !parentScreenIsListItems ? const FilterScreenDivider() : Container(),
              !parentScreenIsListItems
                  ? FilterScreenOnlyTitleSection(filter: state.filter)
                  : Container(),
              !parentScreenIsListItems ? const FilterScreenDivider() : Container(),
              const FilterScreenTitleSection(
                  text: Strings.filterScreenCategorySectionTitle),
              const FilterScreenDivider(),
              FilterScreenRadioBtnsCategory(filter: state.filter),
              const FilterScreenTitleSection(
                  text: Strings.filterScreenRegionSectionTitle),
              const FilterScreenDivider(),
              FilterScreenRadioBtnsRegion(filter: state.filter),
              const FilterScreenTitleSection(
                  text: Strings.filterScreenSortSectionTitle),
              const FilterScreenDivider(),
              FilterScreenRadioBtnsSort(filter: state.filter),
              const SizedBox(height: 64)
            ],
          ),
        );
      }
      return Container();
    });
  }
}

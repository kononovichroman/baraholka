import 'package:flutter/material.dart';

class FilterScreenTitleSection extends StatelessWidget {
  final String text;

  const FilterScreenTitleSection({Key? key, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, left: 16, bottom: 16),
      child: Text(text,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black)),
    );
  }
}

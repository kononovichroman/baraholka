import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FilterScreenDivider.dart';

class FilterScreenRadioBtnsRegion extends StatelessWidget{
  final Filter filter;

  const FilterScreenRadioBtnsRegion({Key? key, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: Region.values.map(
            (region) {
          return Column(
            children: [
              Material(
                child: RadioListTile<Region>(
                  key: Key(region.toString()),
                  value: region,
                  groupValue: filter.region,
                  onChanged: (newValue) {
                    if (newValue != null)
                      BlocProvider.of<FilterBloc>(context)
                          .add(FilterClickRegionEvent(indexRegion: newValue));
                  },
                  title: Text(region.text),
                  tileColor: CustomColors.kWhiteColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 16),
                ),
              ),
              const FilterScreenDivider()
            ],
          );
        },
      ).toList(),
    );
  }
}
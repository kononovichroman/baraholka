import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FilterScreenOnlyTitleSection extends StatelessWidget{
  final Filter filter;

  const FilterScreenOnlyTitleSection({Key? key, required this.filter}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListTile(
        onTap: () {
          BlocProvider.of<FilterBloc>(context).add(FilterClickOnlyTitleEvent());
        },
        trailing: Switch.adaptive(
            value: filter.onlyTitle,
            onChanged: (newValue) => {
              BlocProvider.of<FilterBloc>(context)
                  .add(FilterClickOnlyTitleEvent())
            }),
        title: Text(Strings.filterScreenOnlyTitleSection),
        contentPadding: EdgeInsets.symmetric(horizontal: 16),
        tileColor: CustomColors.kWhiteColor,
      ),
    );
  }

}
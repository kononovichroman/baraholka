import 'dart:io';

import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/core/bloc/filter/FilterState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveActionIcon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FilterScreenAppbarAction extends StatelessWidget {
  const FilterScreenAppbarAction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilterBloc, FilterState>(builder: (context, state) {
      if (state is FilterLoadedState) {
        bool isActiveBtn = !BlocProvider.of<FilterBloc>(context)
            .filterRepo
            .isDefaultFilter(state.filter);
        return AdaptiveActionIcon(
            icon: Icon(
                Platform.isIOS ? CupertinoIcons.delete_solid : Icons.delete),
            onPressed: isActiveBtn
                ? () {
                    BlocProvider.of<FilterBloc>(context)
                        .add(FilterClickClearBtn());
                  }
                : null);
      }
      return AdaptiveActionIcon(
          icon:
              Icon(Platform.isIOS ? CupertinoIcons.delete_solid : Icons.delete),
          onPressed: null);
    });
  }
}

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/ext/adaptive/AdaptiveButton.dart';
import 'package:flutter/material.dart';

class FilterScreenApplyBtn extends StatelessWidget {
  final VoidCallback? onTap;

  const FilterScreenApplyBtn({Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: AdaptiveButton.raised(
          child: Text(Strings.filterScreenApplyBtn),
          color: CustomColors.kMainBlueColor,
          onPressed: onTap,
        ),
      ),
    );
  }
}

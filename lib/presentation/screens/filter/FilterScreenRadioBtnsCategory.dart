import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/presentation/screens/filter/FilterScreenDivider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FilterScreenRadioBtnsCategory extends StatelessWidget {
  final Filter filter;

  const FilterScreenRadioBtnsCategory({Key? key, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: Category.values.map(
        (category) {
          return Column(
            children: [
              Material(
                child: RadioListTile<Category>(
                  key: Key(category.toString()),
                  value: category,
                  groupValue: filter.category,
                  onChanged: (newValue) {
                    if (newValue != null)
                      BlocProvider.of<FilterBloc>(context)
                          .add(FilterClickCategoryEvent(indexCategory: newValue));
                  },
                  title: Text(category.text),
                  tileColor: CustomColors.kWhiteColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 16),
                ),
              ),
              const FilterScreenDivider()
            ],
          );
        },
      ).toList(),
    );
  }
}

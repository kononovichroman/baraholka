import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FilterScreenDivider extends StatelessWidget {
  const FilterScreenDivider();

  @override
  Widget build(BuildContext context) {
    return Divider(
        height: 1,
        color: Platform.isIOS ? CupertinoColors.separator : Colors.black12);
  }
}

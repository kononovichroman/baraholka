import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FilterScreenAppbarAction.dart';
import 'FilterScreenApplyBtn.dart';
import 'FilterScreenBody.dart';

class FilterScreen extends StatelessWidget {
  static var routeName = '/filter';

  const FilterScreen();

  @override
  Widget build(BuildContext context) {
    RouteSettings? settings = ModalRoute.of(context)?.settings;
    bool parentScreenIsListItems = settings?.arguments != null;

    return AdaptiveScaffold(
      cupertinoNavigationBar: CupertinoNavigationBar(
        middle: Text(Strings.filterScreenAppBarTitle,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: CustomColors.kWhiteColor)),
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        trailing: const FilterScreenAppbarAction(),
      ),
      appBar: AppBar(
        title: Text(Strings.filterScreenAppBarTitle,
            style: TextStyle(color: CustomColors.kWhiteColor)),
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        actions: [const FilterScreenAppbarAction()],
      ),
      body: Stack(
        children: [
          FilterScreenBody(parentScreenIsListItems),
          SafeArea(
            child: Container(
                alignment: Alignment.bottomCenter,
                child: FilterScreenApplyBtn(
                  onTap: () {
                    BlocProvider.of<FilterBloc>(context)
                      ..add(FilterClickApplyBtn());
                    Navigator.pop(context, "apply");
                  },
                )),
          )
        ],
      ),
    );
  }
}

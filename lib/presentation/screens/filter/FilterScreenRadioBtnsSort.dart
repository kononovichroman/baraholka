import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/filter/FilterBloc.dart';
import 'package:baraholka/core/bloc/filter/FilterEvent.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FilterScreenDivider.dart';

class FilterScreenRadioBtnsSort extends StatelessWidget {
  final Filter filter;

  const FilterScreenRadioBtnsSort({Key? key, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: Sort.values.map(
        (sort) {
          return Column(
            children: [
              Material(
                child: RadioListTile<Sort>(
                  key: Key(sort.toString()),
                  value: sort,
                  groupValue: filter.sort,
                  onChanged: (newValue) {
                    if (newValue != null)
                      BlocProvider.of<FilterBloc>(context)
                          .add(FilterClickSortEvent(indexSort: newValue));
                  },
                  title: Text(sort.text),
                  tileColor: CustomColors.kWhiteColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                  contentPadding: EdgeInsets.symmetric(horizontal: 16),
                ),
              ),
              const FilterScreenDivider()
            ],
          );
        },
      ).toList(),
    );
  }
}

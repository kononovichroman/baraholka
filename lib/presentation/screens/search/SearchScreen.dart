import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/search/SearchBloc.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/search/SearchScreenBody.dart';
import 'package:baraholka/presentation/screens/search/SearchScreenSearchWidget.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';


class SearchScreen extends StatefulWidget {
  final ItemsRepo itemsRepo;
  final FilterRepo filterRepo;

  const SearchScreen({required this.itemsRepo, required this.filterRepo});

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late SearchBloc _searchBloc;
  late PagingController<int, Item> _pagingController;

  @override
  void initState() {
    _searchBloc = SearchBloc(widget.itemsRepo, widget.filterRepo);
    _pagingController = PagingController<int, Item>(
      firstPageKey: 1,
    );
    super.initState();
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SearchBloc>(
        create: (context) => _searchBloc,
        child: AdaptiveScaffold(
            backgroundColor: CustomColors.kMainBlueColor,
            appBar: AppBar(
              toolbarHeight: 0,
              brightness: Brightness.dark,
              shadowColor: Colors.transparent,
            ),
            body: SafeArea(
              child: Container(
                decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
                child: Column(
                  children: [
                    SearchScreenSearchWidget(
                      pagingController: _pagingController,
                      returnData: () => _pagingController.refresh(),
                    ),
                    SearchScreenBody(pagingController: _pagingController)
                  ],
                ),
              ),
            )));
  }
}

import 'dart:io';

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/search/SearchBloc.dart';
import 'package:baraholka/core/bloc/search/SearchEvent.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Debouncer.dart';
import 'package:baraholka/ext/adaptive/AdaptiveTextField.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreenFilterBtn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class SearchScreenSearchWidget extends StatefulWidget {
  final PagingController<int, Item> pagingController;
  final VoidCallback? returnData;

  const SearchScreenSearchWidget(
      {required this.pagingController, this.returnData});

  @override
  _SearchScreenSearchWidgetState createState() =>
      _SearchScreenSearchWidgetState();
}

class _SearchScreenSearchWidgetState extends State<SearchScreenSearchWidget> {
  late TextEditingController _controller;
  late Debouncer _debouncer;

  @override
  void initState() {
    super.initState();

    _controller = TextEditingController();
    _controller.addListener(() {
      setState(() {});
    });
    _debouncer = Debouncer(milliseconds: 500);
    widget.pagingController.addPageRequestListener((pageKey) {
      BlocProvider.of<SearchBloc>(context).add(
          SearchLoadEvent(searchText: _controller.text, nextPage: pageKey));
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.kMainBlueColor,
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
            height: 36,
            width: MediaQuery.of(context).size.width - 64,
            decoration: const BoxDecoration(
                borderRadius:
                    const BorderRadius.all(const Radius.circular(10)),
                color: CustomColors.kMainGrayColor),
            child: AdaptiveTextField(
              textInputAction: TextInputAction.search,
              maxLines: 1,
              onChanged: (text) {
                _debouncer.run(() {
                  BlocProvider.of<SearchBloc>(context)
                      .add(SearchNewLoadEvent(searchText: text));
                });
              },
              autofocus: true,
              controller: _controller,
              cupertinoDecoration: const BoxDecoration(
                  borderRadius: const BorderRadius.all(
                      const Radius.circular(10)),
                  color: CustomColors.kMainGrayColor),
              textAlignVertical: TextAlignVertical.center,
              placeholder: Strings.textFieldHintText,
              prefix: Row(
                children: [
                  const SizedBox(width: 8),
                  const Icon(
                    CupertinoIcons.search,
                    color: CustomColors.kIconGrayColor,
                  ),
                ],
              ),
              suffix: _controller.text.isNotEmpty
                  ? Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            _controller.clear();
                            BlocProvider.of<SearchBloc>(context).add(
                                SearchNewLoadEvent(
                                    searchText: _controller.text));
                          },
                          child: const Icon(
                            CupertinoIcons.clear_circled_solid,
                            color: CupertinoColors.systemGrey,
                            size: 20,
                          ),
                        ),
                        const SizedBox(width: 8)
                      ],
                    )
                  : null,
              placeholderStyle: const TextStyle(
                textBaseline: TextBaseline.alphabetic,
                  color: CustomColors.kIconGrayColor, fontSize: 16),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Strings.textFieldHintText,
                  prefixIcon: const Icon(
                    Icons.search,
                    color: CustomColors.kIconGrayColor,
                  ),
                  suffixIcon: _controller.text.isNotEmpty
                      ? IconButton(
                          icon: const Icon(
                            Icons.clear,
                            color: Colors.grey,
                            size: 20,
                          ),
                          onPressed: () {
                            _controller.clear();
                            BlocProvider.of<SearchBloc>(context).add(
                                SearchNewLoadEvent(
                                    searchText: _controller.text));
                          })
                      : null,
                  isCollapsed: true,
                  hintStyle: const TextStyle(
                      color: CustomColors.kIconGrayColor,
                      fontSize: 16)),
              cursorColor: CustomColors.kMainBlueColor,
            ),
          ),
          Platform.isIOS ? const SizedBox(width: 8) : const SizedBox(),
          ListItemsScreenFilterBtn(
            returnData: () {
              widget.pagingController.refresh();
            },
          )
        ],
      ),
    );
  }
}

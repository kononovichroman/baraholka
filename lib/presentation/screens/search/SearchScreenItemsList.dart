import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreenItemWidget.dart';
import 'package:baraholka/presentation/widgets/ErrorNetworkWidget.dart';
import 'package:baraholka/presentation/widgets/FirstPageProgressIndicator.dart';
import 'package:baraholka/presentation/widgets/NewPageErrorIndicator.dart';
import 'package:baraholka/presentation/widgets/NewPageProgressIndicator.dart';
import 'package:baraholka/presentation/widgets/NotFoundPageWidget.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';


class SearchScreenItemsList extends StatelessWidget{
  const SearchScreenItemsList({Key? key, required this.pagingController, this.loadedItems, this.isNewSearch}): super(key: key);
  final Pair<List<Item>, int?>? loadedItems;
  final PagingController<int, Item> pagingController;
  final bool? isNewSearch;
  @override
  Widget build(BuildContext context) {

    if(loadedItems != null) {
      if(isNewSearch ?? false) {
        pagingController.refresh();
      }
      pagingController.appendPage(loadedItems!.first, loadedItems!.second);
    } else {
      if(isNewSearch ?? false) {
        pagingController.refresh();
      }
      pagingController.error = '';
    }

    return Expanded(
      child: Scrollbar(
        child: PagedListView<int, Item>.separated(
            pagingController: pagingController,
            builderDelegate: PagedChildBuilderDelegate<Item>(
                itemBuilder: (context, item, index) =>
                    ListItemsScreenItemWidget(item: item, index: index),
                newPageProgressIndicatorBuilder: (_) =>
                    NewPageProgressIndicator(),
                newPageErrorIndicatorBuilder: (_) => NewPageErrorIndicator(
                  onTap: pagingController.retryLastFailedRequest,
                ),
                firstPageErrorIndicatorBuilder: (_) => ErrorNetworkWidget(
                  onTap: pagingController.refresh,
                ),
                noItemsFoundIndicatorBuilder: (_) => NotFoundPageWidget(),
                firstPageProgressIndicatorBuilder: (_) =>
                    FirstPageProgressIndicator()),
            separatorBuilder: (context, index) => Container()),
      ),
    );
  }

}
import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/core/bloc/search/SearchBloc.dart';
import 'package:baraholka/core/bloc/search/SearchState.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/presentation/screens/search/SearchScreenItemsList.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'SearchScreenStubBody.dart';

class SearchScreenBody extends StatelessWidget {
  final PagingController<int, Item> pagingController;

  const SearchScreenBody({Key? key, required this.pagingController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {

        if (state is SearchLoadedState) {
          return Expanded(
            child: Container(
              decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
              child: Column(
                children: [
                  SearchScreenItemsList(
                    pagingController: pagingController,
                    loadedItems: state.loadedItems,
                    isNewSearch: state.isNewSearch,
                  ),
                ],
              ),
            ),
          );
        }
        if (state is SearchErrorState) {
          return Expanded(
            child: Container(
              decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
              child: Column(
                children: [
                  SearchScreenItemsList(
                    pagingController: pagingController, loadedItems: null,isNewSearch: state.isNewSearch,),
                ],
              ),
            ),
          );
        }
        if (state is SearchLoadingState) {
          return Expanded(
              child: Center(child: CircularProgressIndicator.adaptive()));
        }
        if (state is SearchStartState) {
          return SearchScreenStubBody();
        }
        return Container();
      },
    );
  }
}

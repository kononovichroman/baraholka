import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/main/MainScreenBody.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({
    Key? key,
    this.returnData,
  }) : super(key: key);
  final VoidCallback? returnData;

  @override
  Widget build(BuildContext context) {
    return AdaptiveScaffold(
      cupertinoNavigationBar: CupertinoNavigationBar(
        middle: Text(Strings.baraholkaTitle,
            style: TextStyle(
                fontWeight: FontWeight.w900, color: CustomColors.kWhiteColor)),
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        border: null,
      ),
      appBar: AppBar(
          title: Text(Strings.baraholkaTitle,
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  color: CustomColors.kWhiteColor)),
          centerTitle: true,
          backgroundColor: CustomColors.kMainBlueColor,
          shadowColor: Colors.transparent,
          brightness: Brightness.dark),
      body: MainScreenBody(returnData: returnData,),
    );
  }
}

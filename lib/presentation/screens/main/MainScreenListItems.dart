import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/data/model/Categories.dart';
import 'package:flutter/material.dart';

class MainScreenListItems extends StatelessWidget {
  MainScreenListItems({
    Key? key,
    this.returnData,
  }) : super(key: key);
  final Function(String name, int index)? returnData;

  final List<Categories> listCategories = Categories.initializeDataList();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.separated(
        itemCount: listCategories.length,
        itemBuilder: (context, index) {
          return _itemCategory(context, listCategories, index);
        },
        separatorBuilder: (context, index) {
          return _itemSeparator();
        },
      ),
    );
  }

  Widget _itemCategory(BuildContext context, List<Categories> list, int index) {
    return Material(
      child: ListTile(
        onTap: () {
          returnData?.call(list[index].name, index);
        },
        title: Text(
          list[index].name,
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 24),
        tileColor: CustomColors.kWhiteColor,
        leading: Icon(
          list[index].icon,
          color: CustomColors.kMainBlueColor,
        ),
      ),
    );
  }

  Widget _itemSeparator() {
    return Container(
        decoration: BoxDecoration(color: CustomColors.kWhiteColor),
        child: Divider(
          indent: 68,
          height: 1,
        ));
  }
}

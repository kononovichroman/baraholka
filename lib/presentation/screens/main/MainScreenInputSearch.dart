import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/ext/adaptive/AdaptiveTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MainScreenInputSearch extends StatelessWidget {
  const MainScreenInputSearch({
    Key? key,
    this.returnData,
  }) : super(key: key);
  final VoidCallback? returnData;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        returnData?.call();
      },
      child: Container(
        color: CustomColors.kMainBlueColor,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          height: 36,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: CustomColors.kMainGrayColor),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: AdaptiveTextField(
                  cupertinoDecoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: CustomColors.kMainGrayColor),
                  enabled: false,
                  textAlignVertical: TextAlignVertical.center,
                  placeholder: Strings.textFieldHintText,
                  prefix: Row(
                    children: [
                      SizedBox(width: 8),
                      Icon(
                        CupertinoIcons.search,
                        color: CustomColors.kIconGrayColor,
                      ),
                    ],
                  ),
                  placeholderStyle: TextStyle(
                      color: CustomColors.kIconGrayColor, fontSize: 16),
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: Strings.textFieldHintText,
                      prefixIcon: Icon(
                        Icons.search,
                        color: CustomColors.kIconGrayColor,
                      ),
                      isCollapsed: true,
                      hintStyle: TextStyle(
                          color: CustomColors.kIconGrayColor, fontSize: 16)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

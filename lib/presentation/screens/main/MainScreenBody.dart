import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/presentation/screens/main/MainScreenInputSearch.dart';
import 'package:baraholka/presentation/screens/main/MainScreenListItems.dart';
import 'package:baraholka/presentation/screens/subcategory/SubcategoryScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MainScreenBody extends StatelessWidget {
  const MainScreenBody({
    Key? key,
    this.returnData,
  }) : super(key: key);
  final VoidCallback? returnData;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
      child: Column(
        children: [
          MainScreenInputSearch(
            returnData: returnData,
          ),
          MainScreenListItems(
            returnData: (name, index) {
              Pair<String, int> pair =
                  Pair<String, int>(first: name, second: index);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SubcategoryScreen(),
                      settings: RouteSettings(
                          name: SubcategoryScreen.routeName, arguments: pair)));
            },
          )
        ],
      ),
    );
  }
}

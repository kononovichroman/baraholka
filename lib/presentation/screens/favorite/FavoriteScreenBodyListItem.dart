import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteState.dart';
import 'package:baraholka/presentation/screens/listitems/ListItemsScreenItemWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:baraholka/core/db/FavoriteItem.dart';

class FavoriteScreenBodyListItem extends StatelessWidget {
  const FavoriteScreenBodyListItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteBloc, FavoriteState>(builder: (context, state) {
      if (state is FavoriteLoadedState) {
        final listFavoriteItems = state.listFavoriteItems;
        return Scrollbar(
            child: ListView.separated(
                itemBuilder: (context, index) {
                  return ListItemsScreenItemWidget(item: listFavoriteItems[index].convertItem, index: index);
                },
                separatorBuilder: (context, index) {
                  return Container();
                },
                itemCount: listFavoriteItems.length));
      }
      return Container();
    });
  }
}

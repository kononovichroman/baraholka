import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteState.dart';
import 'package:baraholka/presentation/screens/favorite/FavoriteScreenBodyListItem.dart';
import 'package:baraholka/presentation/screens/favorite/FavoriteScreenStubBody.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoriteScreenBody extends StatelessWidget {
  const FavoriteScreenBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteBloc, FavoriteState>(builder: (context, state) {
      if (state is FavoriteEmptyState) {
        return const FavoriteScreenStubBody();
      }
      if (state is FavoriteLoadedState) {
        return const FavoriteScreenBodyListItem();
      }
      return FavoriteScreenStubBody();
    });
  }
}

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteEvent.dart';
import 'package:baraholka/ext/adaptive/AdaptiveAlertDialogAction.dart';
import 'package:baraholka/ext/adaptive/AdaptiveScaffold.dart';
import 'package:baraholka/presentation/screens/favorite/FavoriteScreenActionClear.dart';
import 'package:baraholka/presentation/screens/favorite/FavoriteScreenBody.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdaptiveScaffold(
      cupertinoNavigationBar: CupertinoNavigationBar(
        middle: Text(Strings.favoriteScreenAppBar,
            style: TextStyle(color: CustomColors.kWhiteColor)),
        backgroundColor: CustomColors.kMainBlueColor,
        brightness: Brightness.dark,
        border: null,
        trailing: const FavoriteScreenActionClear(),
      ),
      appBar: AppBar(
          title: Text(Strings.favoriteScreenAppBar,
              style: TextStyle(color: CustomColors.kWhiteColor)),
          backgroundColor: CustomColors.kMainBlueColor,
          shadowColor: Colors.transparent,
          actions: [const FavoriteScreenActionClear()],
          brightness: Brightness.dark),
      body: const FavoriteScreenBody(),
    );
  }
}

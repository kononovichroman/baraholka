import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/presentation/icons/CustomIcons.dart';
import 'package:flutter/material.dart';

class FavoriteScreenStubBody extends StatelessWidget {
  const FavoriteScreenStubBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: CustomColors.kMainGrayColor),
      child: Column(
        children: [
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CustomIcons.pixel_favorite,
                    size: 128,
                    color: Colors.black,
                  ),
                  SizedBox.fromSize(
                    size: Size.fromHeight(24),
                  ),
                  Text(
                    Strings.favoriteScreenEmptyTitle,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                  SizedBox.fromSize(
                    size: Size.fromHeight(8),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Text(
                      Strings.favoriteScreenEmptyDesc,
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteBloc.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteEvent.dart';
import 'package:baraholka/core/bloc/favorite/FavoriteState.dart';
import 'package:baraholka/ext/adaptive/AdaptiveActionIcon.dart';
import 'package:baraholka/ext/adaptive/AdaptiveAlertDialogAction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoriteScreenActionClear extends StatelessWidget {
  const FavoriteScreenActionClear({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteBloc, FavoriteState>(builder: (context, state) {
      return AdaptiveActionIcon(
          icon: Icon(Platform.isIOS ? CupertinoIcons.trash : Icons.delete),
          onPressed: state is FavoriteEmptyState
              ? null
              : () async {
                  final result = await showOkCancelAlertDialog(
                      context: context,
                      useActionSheetForCupertino: true,
                      isDestructiveAction: true,
                      message: Strings.favoriteScreenDialogMsg,
                      okLabel: Strings.favoriteScreenDialogActionBtn,
                      cancelLabel: Strings.favoriteScreenDialogCancelBtn);
                  if (result == OkCancelResult.ok) {
                    BlocProvider.of<FavoriteBloc>(context)
                        .add(FavoriteDeleteAllEvent());
                  }
                });
    });
  }
}

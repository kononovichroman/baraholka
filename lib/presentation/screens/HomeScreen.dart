import 'dart:io';

import 'package:baraholka/constants/CustomColors.dart';
import 'package:baraholka/constants/Strings.dart';
import 'package:baraholka/presentation/screens/search/SearchScreen.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'favorite/FavoriteScreen.dart';
import 'main/MainScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<NavigatorState> firstTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> secondTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> thirdTabNavKey = GlobalKey<NavigatorState>();

  int _currentTabIndex = 0;
  late CupertinoTabController _controller;
  late List<Widget> tabs;

  onTapped(int index) {
    setState(() {
      _currentTabIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _controller = CupertinoTabController();
    tabs = [
      MainScreen(returnData: (){
        Platform.isIOS ? _controller.index = 1 : onTapped(1);
      },),
      SearchScreen(itemsRepo: ItemsRepo(), filterRepo: FilterRepo()),
      FavoriteScreen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return CupertinoTabScaffold(
          tabBar: CupertinoTabBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(CupertinoIcons.list_dash),
                  activeIcon: Icon(
                    CupertinoIcons.list_dash,
                    color: CustomColors.kMainBlueColor,
                  )),
              BottomNavigationBarItem(
                  icon: Icon(CupertinoIcons.search),
                  activeIcon: Icon(
                    CupertinoIcons.search,
                    color: CustomColors.kMainBlueColor,
                  )),
              BottomNavigationBarItem(
                  icon: Icon(CupertinoIcons.heart_fill),
                  activeIcon: Icon(
                    CupertinoIcons.heart_fill,
                    color: CustomColors.kMainBlueColor,
                  )),
            ],
            onTap: (index) {
              if(_currentTabIndex == index) {
                switch (index) {
                  case 0:
                    firstTabNavKey.currentState?.popUntil((r) => r.isFirst);
                    break;
                  case 1:
                    secondTabNavKey.currentState?.popUntil((r) => r.isFirst);
                    break;
                  case 2:
                    thirdTabNavKey.currentState?.popUntil((r) => r.isFirst);
                    break;
                }
              }
              _currentTabIndex = index;
            },
          ),
          controller: _controller,
          tabBuilder: (context, index) {
            switch (index) {
              case 0:
                return CupertinoTabView(
                  navigatorKey: firstTabNavKey,
                  builder: (context) {
                    return CupertinoPageScaffold(child: tabs[index]);
                  },
                );
              case 1: return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: tabs[index]);
                },
              );
              case 2: return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: tabs[index]);
                },
              );
              default:
                return tabs[0];
            }
          },);
    } else {
      return Scaffold(
        body: tabs[_currentTabIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentTabIndex,
          onTap: onTapped,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.list), label: Strings.bottomNavBarCategory),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: Strings.bottomNavBarSearch),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite), label: Strings.bottomNavBarFavorite),
          ],
        ),
      );
    }
  }
}

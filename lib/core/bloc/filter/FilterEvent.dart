import 'package:baraholka/data/model/filter/Filter.dart';

abstract class FilterEvent {}

class FilterLoadEvent extends FilterEvent {}

class FilterClickOnlyTitleEvent extends FilterEvent {}

class FilterClickCategoryEvent extends FilterEvent {
  Category indexCategory;

  FilterClickCategoryEvent({required this.indexCategory});
}

class FilterClickRegionEvent extends FilterEvent {
  Region indexRegion;

  FilterClickRegionEvent({required this.indexRegion});
}
class FilterClickSortEvent extends FilterEvent {
  Sort indexSort;

  FilterClickSortEvent({required this.indexSort});
}

class FilterClickApplyBtn extends FilterEvent {
}

class FilterClickClearBtn extends FilterEvent{}

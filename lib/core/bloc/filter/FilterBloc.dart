import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FilterEvent.dart';
import 'FilterState.dart';

class FilterBloc extends Bloc<FilterEvent, FilterState> {
  final FilterRepo filterRepo;

  FilterBloc({required this.filterRepo}) : super(FilterLoadingState());

  late Filter _filter;

  @override
  Stream<FilterState> mapEventToState(FilterEvent event) async* {
    if (event is FilterLoadEvent) {
      try {
        _filter = await filterRepo.getSaveFilter();
        yield FilterLoadedState(filter: _filter);
      } catch (e) {
        yield FilterLoadedState(
            filter: Filter(
                category: Category.ALL,
                region: Region.ALL,
                sort: Sort.ACTUAL,
                onlyTitle: false));
      }
    }
    if (event is FilterClickOnlyTitleEvent) {
      _filter.onlyTitle = !_filter.onlyTitle;
      yield FilterLoadedState(filter: _filter);
    }
    if (event is FilterClickCategoryEvent) {
      _filter.category = event.indexCategory;
      yield FilterLoadedState(filter: _filter);
    }
    if (event is FilterClickRegionEvent) {
      _filter.region = event.indexRegion;
      yield FilterLoadedState(filter: _filter);
    }
    if(event is FilterClickSortEvent){
      _filter.sort = event.indexSort;
      yield FilterLoadedState(filter: _filter);
    }
    if(event is FilterClickApplyBtn){
      filterRepo.setSaveFilter(_filter);
    }

    if(event is FilterClickClearBtn) {
      _filter = filterRepo.getEmptyFilter();
      yield FilterLoadedState(filter: _filter);
    }

  }
}

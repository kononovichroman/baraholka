import 'package:baraholka/data/model/filter/Filter.dart';

abstract class FilterState {}

class FilterLoadingState extends FilterState {}

class FilterLoadedState extends FilterState {
  Filter filter;

  FilterLoadedState({required this.filter});
}

abstract class SearchEvent {}

class SearchLoadEvent extends SearchEvent {
  String _searchText;
  int? _nextPage = 0;

  SearchLoadEvent({required String searchText, int? nextPage})
      : _searchText = searchText,
        _nextPage = nextPage;

  String get searchText => _searchText;

  int get nextPage {
    if (_nextPage != null) {
      return _nextPage!;
    } else {
      return -1;
    }
  }
}

class SearchNewLoadEvent extends SearchEvent {
  String _searchText;

  SearchNewLoadEvent({required String searchText}) : _searchText = searchText;

  String get searchText => _searchText;
}
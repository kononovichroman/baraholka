import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';

abstract class SearchState{}

class SearchLoadingState extends SearchState{}
class SearchStartState extends SearchState {}
class SearchLoadedState extends SearchState{
  Pair<List<Item>, int?> loadedItems;
  bool isNewSearch;
  SearchLoadedState({required this.loadedItems, required this.isNewSearch});
}

class SearchErrorState extends SearchState {
  String? error;
  bool isNewSearch;
  SearchErrorState({required this.error, required this.isNewSearch});
}
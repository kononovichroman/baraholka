import 'package:baraholka/core/bloc/search/SearchEvent.dart';
import 'package:baraholka/core/bloc/search/SearchState.dart';
import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final ItemsRepo _itemsRepo;
  final FilterRepo _filterRepo;

  SearchBloc(this._itemsRepo, this._filterRepo) : super(SearchStartState());

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is SearchLoadEvent) {
      if (event.searchText.isEmpty) {
        yield SearchStartState();
        return;
      }
      try {
        final Filter _filter = await _filterRepo.getSaveFilter();
        final Pair<List<Item>, int?> _loadedItemsList = await _itemsRepo
            .searchItem(event.searchText, event.nextPage, _filter);
        yield SearchLoadedState(
            loadedItems: _loadedItemsList, isNewSearch: false);
      } catch (e) {
        yield SearchErrorState(error: e.toString(), isNewSearch: false);
      }
    }
    if (event is SearchNewLoadEvent) {
      if (event.searchText.isEmpty) {
        yield SearchStartState();
        return;
      } else {
        yield SearchLoadingState();
      }
      try {
        final Filter _filter = await _filterRepo.getSaveFilter();
        final Pair<List<Item>, int?> _loadedItemsList =
            await _itemsRepo.searchItem(event.searchText, 0, _filter);
        yield SearchLoadedState(
            loadedItems: _loadedItemsList, isNewSearch: true);
      } catch (e) {
        yield SearchErrorState(error: e.toString(), isNewSearch: true);
      }
    }
  }
}

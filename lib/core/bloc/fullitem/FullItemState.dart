import 'package:baraholka/data/model/fullitem/FullItem.dart';
import 'package:baraholka/ext/ExceptionType.dart';

abstract class FullItemState {}

class FullItemLoadingState extends FullItemState {}

class FullItemLoadedState extends FullItemState {
  FullItem fullItem;
  bool isFavorite;

  FullItemLoadedState({required this.fullItem,required this.isFavorite});
}

class FullItemErrorState extends FullItemState {
  ExceptionType? error;
  FullItemErrorState({required this.error});

}
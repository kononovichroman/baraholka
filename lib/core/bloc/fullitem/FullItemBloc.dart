import 'package:baraholka/data/model/fullitem/FullItem.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/ExceptionType.dart';
import 'package:baraholka/services/favorite/FavoriteRepo.dart';
import 'package:baraholka/services/favorite/FavoriteRepoAPI.dart';
import 'package:baraholka/services/fullitem/FullItemRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FullItemEvent.dart';
import 'FullItemState.dart';

class FullItemBloc extends Bloc<FullItemEvent, FullItemState> {
  final FullItemRepo _fullItemRepo;
  final FavoriteRepoAPI _favoriteRepo;

  FullItemBloc(this._fullItemRepo, this._favoriteRepo)
      : super(FullItemLoadingState());

  FullItem? _fullItem;
  late Item _item;

  @override
  Stream<FullItemState> mapEventToState(FullItemEvent event) async* {
    if (event is FullItemLoadEvent || event is FullItemRetryLoadEvent) {
      if (event is FullItemLoadEvent) {
        _item = event.item;
      } else if (event is FullItemRetryLoadEvent) {
        yield FullItemLoadingState();
      }
      try {
        _fullItem = await _fullItemRepo.getFullItem(_item.id);
        final bool isFavorite = await _favoriteRepo.isFavorite(_fullItem!.link);
        yield FullItemLoadedState(fullItem: _fullItem!, isFavorite: isFavorite);
      } catch (e) {
        if (e is ExceptionType) {
          if (e is ExceptionTypeNotFound) {
            try {
              await _favoriteRepo.removeFromFavorite(_item);
            } catch (e) {
              print(e.toString());
            }
          }
          yield FullItemErrorState(error: e);
        } else {
          yield FullItemErrorState(error: ExceptionTypeNetwork());
        }
      }
    }
    if (event is FullItemClickFavoriteAction) {
      try {
        await _favoriteRepo.isFavorite(_item.link)
            ? _favoriteRepo.removeFromFavorite(_item)
            : _favoriteRepo.addToFavorite(_item);

        final bool isFavorite = await _favoriteRepo.isFavorite(_item.link);
        yield FullItemLoadedState(fullItem: _fullItem!, isFavorite: isFavorite);
      } catch (e) {}
    }
  }
}

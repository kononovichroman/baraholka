import 'package:baraholka/data/model/item/Item.dart';

abstract class FullItemEvent {}

class FullItemLoadEvent extends FullItemEvent {
  Item item;

  FullItemLoadEvent({required this.item});
}

class FullItemRetryLoadEvent extends FullItemEvent {}

class FullItemClickFavoriteAction extends FullItemEvent {
  FullItemClickFavoriteAction();
}

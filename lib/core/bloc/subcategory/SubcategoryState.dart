import 'package:baraholka/data/model/Subcategory.dart';

abstract class SubcategoryState {}

class SubcategoryLoadingState extends SubcategoryState {}

class SubcategoryLoadedState extends SubcategoryState {
  List<Subcategory> loadedSubcategory;

  SubcategoryLoadedState({required this.loadedSubcategory});
}

class SubcategoryErrorState extends SubcategoryState {}

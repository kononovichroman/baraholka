import 'package:baraholka/data/model/Subcategory.dart';
import 'package:baraholka/services/subcategory/SubcategoryRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SubcategoryEvent.dart';
import 'SubcategoryState.dart';

class SubcategoryBloc extends Bloc<SubcategoryEvent, SubcategoryState> {
  final SubcategoryRepo subcategoryRepo;

  SubcategoryBloc(this.subcategoryRepo) : super(SubcategoryLoadingState());
  int lastIdCategory = 0;

  @override
  Stream<SubcategoryState> mapEventToState(SubcategoryEvent event) async* {
    if (event is SubcategoryLoadEvent) {
      yield SubcategoryLoadingState();
      try {
        lastIdCategory = event.idCategory;
        final List<Subcategory> _loadedSubcategoryList =
            await subcategoryRepo.getAllSubcategory(lastIdCategory);
        yield SubcategoryLoadedState(loadedSubcategory: _loadedSubcategoryList);
      } catch (_) {
        yield SubcategoryErrorState();
      }
    }
  }
}

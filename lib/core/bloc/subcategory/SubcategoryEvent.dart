import 'package:baraholka/data/model/Subcategory.dart';

abstract class SubcategoryEvent {}

const String _helperStringForCalcId = 'php?f=';

class SubcategoryLoadEvent extends SubcategoryEvent {
  int _id;

  SubcategoryLoadEvent({required int idCategory}) : _id = idCategory;

  int get idCategory => _id;
}

class SubcategoryClickEvent extends SubcategoryEvent {
  Subcategory _item;

  SubcategoryClickEvent({required Subcategory item}) : _item = item;

  int get idSubcategory {
    return int.parse(_item.link.substring(
        _item.link.indexOf(_helperStringForCalcId) +
            _helperStringForCalcId.length));
  }
}

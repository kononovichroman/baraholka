import 'package:baraholka/core/db/FavoriteItem.dart';

abstract class FavoriteEvent {}

class FavoriteLoadEvent extends FavoriteEvent {}

class FavoriteDeleteAllEvent extends FavoriteEvent {}

class FavoriteClickEvent extends FavoriteEvent {
  FavoriteItem _item;

  FavoriteClickEvent({required FavoriteItem item}) : _item = item;
}

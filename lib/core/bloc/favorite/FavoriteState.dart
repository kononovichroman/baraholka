import 'package:baraholka/core/db/FavoriteItem.dart';

abstract class FavoriteState {}

class FavoriteLoadingState extends FavoriteState {}

class FavoriteLoadedState extends FavoriteState {
  List<FavoriteItem> listFavoriteItems;

  FavoriteLoadedState({required this.listFavoriteItems});
}

class FavoriteEmptyState extends FavoriteState {}

class FavoriteErrorState extends FavoriteState {}

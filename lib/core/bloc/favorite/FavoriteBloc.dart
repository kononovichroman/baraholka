import 'package:baraholka/core/db/FavoriteItem.dart';
import 'package:baraholka/services/favorite/FavoriteRepoAPI.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'FavoriteEvent.dart';
import 'FavoriteState.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  final FavoriteRepoAPI _favoriteRepo;

  FavoriteBloc(this._favoriteRepo) : super(FavoriteLoadingState());

  @override
  Stream<FavoriteState> mapEventToState(FavoriteEvent event) async* {
    if (event is FavoriteLoadEvent) {
      print('er');
      yield FavoriteLoadingState();
      try {
        final List<FavoriteItem> _favoriteList =
        await _favoriteRepo.getAllFavoritesItems();
        if (_favoriteList.isEmpty) {
          yield FavoriteEmptyState();
        } else {
          yield FavoriteLoadedState(listFavoriteItems: _favoriteList);
        }
      } catch (e, stacktrace) {
        print(stacktrace);
        yield FavoriteErrorState();
      }
    }

    if(event is FavoriteDeleteAllEvent){
      try {
        await _favoriteRepo.deleteAll();
        yield FavoriteEmptyState();
      } catch (_) {
        yield FavoriteErrorState();
      }
    }
  }
}

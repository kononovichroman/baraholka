import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:baraholka/services/items/ItemsRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'ItemsEvent.dart';
import 'ItemsState.dart';

class ItemsBloc extends Bloc<ItemsEvent, ItemsState> {
  final ItemsRepo itemsRepo;
  final FilterRepo _filterRepo;

  ItemsBloc(this.itemsRepo, this._filterRepo) : super(ItemsLoadingState());

  @override
  Stream<ItemsState> mapEventToState(ItemsEvent event) async* {
    if (event is ItemsLoadEvent) {
      try {
        final Filter _filter = await _filterRepo.getSaveFilter();

        final Pair<List<Item>, int?> _loadedItemsList = await itemsRepo
            .getAllItems(event.idSubCategory, event.nextPage, _filter);

        yield ItemsLoadedState(loadedItems: _loadedItemsList);
      } catch (e) {
        yield ItemsErrorState(error: e.toString());
      }
    }
  }
}
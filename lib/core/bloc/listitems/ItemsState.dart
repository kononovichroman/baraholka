import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';

abstract class ItemsState {}

class ItemsLoadingState extends ItemsState {}

class ItemsLoadedState extends ItemsState {
  Pair<List<Item>, int?> loadedItems;

  ItemsLoadedState({required this.loadedItems});
}

class ItemsErrorState extends ItemsState {
  String? error;

  ItemsErrorState({required this.error});
}

abstract class ItemsEvent {}

class ItemsLoadEvent extends ItemsEvent {
  int _id;
  int? _nextPage = 0;

  ItemsLoadEvent({required int idSubCategory, int? nextPage})
      : _id = idSubCategory,
        _nextPage = nextPage;

  int get idSubCategory => _id;

  int get nextPage {
    if (_nextPage != null) {
      return _nextPage!;
    } else {
      return -1;
    }
  }
}

abstract class ItemsFilterState{}

class ItemsFilterLoadingState extends ItemsFilterState{}
class ItemsFilterEmptyState extends ItemsFilterState{}
class ItemsFilterNonEmptyState extends ItemsFilterState{}

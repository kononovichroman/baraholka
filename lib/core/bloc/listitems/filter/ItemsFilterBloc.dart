import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/services/filter/FilterRepo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'ItemsFilterLoadEvent.dart';
import 'ItemsFilterState.dart';

class ItemsFilterBloc extends Bloc<ItemsFilterEvent, ItemsFilterState> {
  final FilterRepo _filterRepo;

  ItemsFilterBloc(this._filterRepo) : super(ItemsFilterLoadingState());

  @override
  Stream<ItemsFilterState> mapEventToState(ItemsFilterEvent event) async* {
    if (event is ItemsFilterLoadEvent) {
      final Filter _filter = await _filterRepo.getSaveFilter();
      if (_filterRepo.isDefaultFilter(_filter)) {
        yield ItemsFilterEmptyState();
      } else {
        yield ItemsFilterNonEmptyState();
      }
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FavoriteItemType.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavoriteItemTypeAdapter extends TypeAdapter<FavoriteItemType> {
  @override
  final int typeId = 1;

  @override
  FavoriteItemType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return FavoriteItemType.IMPORTANT;
      case 1:
        return FavoriteItemType.BUY;
      case 2:
        return FavoriteItemType.SELL;
      case 3:
        return FavoriteItemType.CLOSE;
      case 4:
        return FavoriteItemType.RENT;
      case 5:
        return FavoriteItemType.SERVICE;
      case 6:
        return FavoriteItemType.SWAP;
      default:
        return FavoriteItemType.IMPORTANT;
    }
  }

  @override
  void write(BinaryWriter writer, FavoriteItemType obj) {
    switch (obj) {
      case FavoriteItemType.IMPORTANT:
        writer.writeByte(0);
        break;
      case FavoriteItemType.BUY:
        writer.writeByte(1);
        break;
      case FavoriteItemType.SELL:
        writer.writeByte(2);
        break;
      case FavoriteItemType.CLOSE:
        writer.writeByte(3);
        break;
      case FavoriteItemType.RENT:
        writer.writeByte(4);
        break;
      case FavoriteItemType.SERVICE:
        writer.writeByte(5);
        break;
      case FavoriteItemType.SWAP:
        writer.writeByte(6);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavoriteItemTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

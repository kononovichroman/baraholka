// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FavoriteItem.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavoriteItemAdapter extends TypeAdapter<FavoriteItem> {
  @override
  final int typeId = 0;

  @override
  FavoriteItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FavoriteItem(
      title: fields[0] as String,
      description: fields[1] as String,
      srcUrl: fields[2] as String,
      price: fields[3] as String,
      isTorg: fields[4] as bool,
      city: fields[5] as String,
      link: fields[6] as String,
      isPremium: fields[7] as bool,
      type: fields[8] as FavoriteItemType,
      id: fields[9] as int,
    );
  }

  @override
  void write(BinaryWriter writer, FavoriteItem obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.description)
      ..writeByte(2)
      ..write(obj.srcUrl)
      ..writeByte(3)
      ..write(obj.price)
      ..writeByte(4)
      ..write(obj.isTorg)
      ..writeByte(5)
      ..write(obj.city)
      ..writeByte(6)
      ..write(obj.link)
      ..writeByte(7)
      ..write(obj.isPremium)
      ..writeByte(8)
      ..write(obj.type)
      ..writeByte(9)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavoriteItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

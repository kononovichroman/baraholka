import 'package:baraholka/data/model/item/TypeItem.dart';
import 'package:hive/hive.dart';

part 'FavoriteItemType.g.dart';

@HiveType(typeId: 1)
enum FavoriteItemType {
  @HiveField(0)
  IMPORTANT,
  @HiveField(1)
  BUY,
  @HiveField(2)
  SELL,
  @HiveField(3)
  CLOSE,
  @HiveField(4)
  RENT,
  @HiveField(5)
  SERVICE,
  @HiveField(6)
  SWAP
}

extension ItemTypeMapperExtension on TypeItem {
  FavoriteItemType get convertType {
    switch (this) {
      case TypeItem.IMPORTANT:
        return FavoriteItemType.IMPORTANT;
      case TypeItem.BUY:
        return FavoriteItemType.BUY;
      case TypeItem.SELL:
        return FavoriteItemType.SELL;
      case TypeItem.CLOSE:
        return FavoriteItemType.CLOSE;
      case TypeItem.RENT:
        return FavoriteItemType.RENT;
      case TypeItem.SERVICE:
        return FavoriteItemType.SERVICE;
      case TypeItem.SWAP:
        return FavoriteItemType.SWAP;
    }
  }
}
extension FavoriteItemTypeMapperExtension on FavoriteItemType {
  TypeItem get convertType {
    switch (this) {
      case FavoriteItemType.IMPORTANT:
        return TypeItem.IMPORTANT;
      case FavoriteItemType.BUY:
        return TypeItem.BUY;
      case FavoriteItemType.SELL:
        return TypeItem.SELL;
      case FavoriteItemType.CLOSE:
        return TypeItem.CLOSE;
      case FavoriteItemType.RENT:
        return TypeItem.RENT;
      case FavoriteItemType.SERVICE:
        return TypeItem.SERVICE;
      case FavoriteItemType.SWAP:
        return TypeItem.SWAP;
    }
  }
}

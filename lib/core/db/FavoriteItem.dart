import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/data/model/item/TypeItem.dart';
import 'package:hive/hive.dart';

import 'FavoriteItemType.dart';

part 'FavoriteItem.g.dart';

@HiveType(typeId: 0)
class FavoriteItem extends HiveObject {
  @HiveField(0)
  String title;
  @HiveField(1)
  String description;
  @HiveField(2)
  String srcUrl;
  @HiveField(3)
  String price;
  @HiveField(4)
  bool isTorg;
  @HiveField(5)
  String city;
  @HiveField(6)
  String link;
  @HiveField(7)
  bool isPremium;
  @HiveField(8)
  FavoriteItemType type;
  @HiveField(9)
  int id;

  FavoriteItem(
      {required this.title,
      required this.description,
      required this.srcUrl,
      required this.price,
      required this.isTorg,
      required this.city,
      required this.link,
      required this.isPremium,
      required this.type,
      required this.id});
}

extension FavoriteItemMapperExtension on FavoriteItem {
  Item get convertItem {
    return Item(
        title: title,
        description: description,
        srcUrl: srcUrl,
        price: price,
        city: city,
        link: link,
        type: type.convertType,
        isPremium: isPremium,
        isTorg: isTorg,
        id: id);
  }
}

import 'package:baraholka/data/model/fullitem/FullItem.dart';

import 'FullItemApiProvider.dart';

class FullItemRepo {
  FullItemApiProvider _fullItemApiProvider = FullItemApiProvider();

  Future<FullItem> getFullItem(int idItem){
    return _fullItemApiProvider.getFullItem(idItem);
  }
}
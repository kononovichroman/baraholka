import 'dart:collection';

import 'package:baraholka/data/model/fullitem/FullItem.dart';
import 'package:baraholka/ext/ExceptionType.dart';
import 'package:flutter/foundation.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

const String _kStartURL = 'baraholka.onliner.by';
const String _kPathURL = '/viewtopic.php';
const String _kStartLinkTopic = 'https://baraholka.onliner.by/viewtopic.php?t=';
const String _kStartAllAds = 'https://baraholka.onliner.by/search.php?type=ufleamarket&id=';

class FullItemApiProvider {
  Future<FullItem> getFullItem(int idItem) async {
    Map<String, dynamic> queryParams = {'t': idItem.toString()};
    Uri uri = Uri.https(_kStartURL, _kPathURL, queryParams);

    final response = await Future.delayed(Duration(milliseconds: 500), () {
      return http.get(uri);
    });

    if (response.statusCode == 200) {
      return compute(_getFullItemObject, response.body);
    } else if (response.statusCode == 404) {
      throw ExceptionTypeNotFound();
    } else {
      throw ExceptionTypeNetwork();
    }
  }
}

FullItem _getFullItemObject(String body) {
  late FullItem fullItem;
  try {
    var document = parse(body);

    if (body.contains("Запрошенной темы не существует")) {
      throw ExceptionTypeNotFound();
    }
    String? city = document.querySelector("strong.fast-desc-region")?.text;
    if (city == null) {
      city = '';
    }

    print('FullItem_city: $city');
    String idItem = document.querySelector("td.bd.numb")?.text ?? '';
    if (idItem.isNotEmpty) {
      idItem = idItem.replaceAll(RegExp('[^0-9]'), '');
    }
    String link = _kStartLinkTopic + idItem;
    print('FullItem_link: $link');

    String descriptionShort = document.querySelector("li.fast-desc")!.text;
    descriptionShort = descriptionShort.replaceFirst(city, "");

    print('FullItem_descriptionShort: $descriptionShort');

    String afterUp = document.querySelectorAll("tr>td")[1].text.trim();
    String countUpText = '0';
    if (afterUp.contains('назад')) {
      countUpText = afterUp.substring(0, afterUp.lastIndexOf("раз")).trim();
      afterUp = afterUp
          .substring(afterUp.indexOf(" ", afterUp.indexOf("раз")))
          .trimLeft();
    }
    int countUp = int.parse(countUpText);
    print('FullItem_afterUp: $afterUp');

    print('FullItem_countUp: $countUpText');

    String title = document.querySelectorAll("h1.title").first.text;

    print('FullItem_title: $title');

    String nameAuthor = document
        .querySelectorAll("big.mtauthor-nickname")
        .first
        .children[0]
        .text
        .trim();

    print('FullItem_nameAuthor: $nameAuthor');

    List<String> srcUrls = [];
    String descriptionFullHtml =
        document.querySelectorAll("div.content").first.outerHtml;
    String descriptionFullText =
        Element.html(descriptionFullHtml.replaceAll('<br>', ' ')).text;
    List<Element> messageElements = document.querySelectorAll(".b-msgpost-txt");
    List<Element> allImgTag =
        messageElements.first.querySelectorAll(".msgpost-img");
    List<Element> allVideoTag =
        messageElements.first.querySelectorAll('p.postvideo');

    allImgTag.forEach((element) {
      String str = element.outerHtml;
      descriptionFullHtml = descriptionFullHtml.replaceAll(str, "");
      if (str.contains('http') && str.contains('content.onliner.by')) {
        String finalSrcUrl = _getUrlImg(str);
        if (!finalSrcUrl.endsWith('.gif')) {
          srcUrls.add(finalSrcUrl);
        }
      }
    });
    allVideoTag.forEach((element) {
      descriptionFullHtml =
          descriptionFullHtml.replaceAll(element.outerHtml, '');
    });
    print('FullItem_srcUrls: $srcUrls');

    descriptionFullHtml =
        descriptionFullHtml.replaceAll(RegExp("<img.+?>"), '');
    descriptionFullHtml = descriptionFullHtml.replaceAll("<div></div>", '');
    print('FullItem_descriptionFullHtml: $descriptionFullHtml');
    print('FullItem_descriptionFullText: $descriptionFullText');

    String infoAuthor = document.querySelectorAll("td.ava-box").first.outerHtml;
    String authorSrc = _getUrlImg(infoAuthor);

    print('FullItem_authorSrc: $authorSrc');

    String? price;
    price = document.querySelector("li.price-primary")?.text;
    bool priceTorg = false;
    priceTorg = document.querySelector('li.torg') != null;
    print('FullItem_price: $price');

    int startIndexInfoAuthor = infoAuthor.indexOf("user/") + 5;
    String allAds = infoAuthor.substring(startIndexInfoAuthor,
        (infoAuthor.indexOf("\">", startIndexInfoAuthor)));
    allAds = _kStartAllAds + allAds;
    String linkWriteAuthor =
        document.querySelectorAll('a.b-mta-send').first.attributes['href']!;
    print('FullItem_linkWriteAuthor: $linkWriteAuthor');

    // Parse number
    HashSet<String> phone = HashSet<String>();
    Pattern pattern =
        RegExp("([^0-9]|\\A)(375|80)(33|44|25|29)[1-9]\\d{6}([^0-9]|\\z)");
    Pattern patternNotFull =
        RegExp('([^0-9]|\\A)(33|44|25|29)[1-9]\\d{6}([^0-9]|\\z)');

    String findInText = descriptionFullText
        .replaceAll(
            RegExp('((?:https?:\\/\\/|www\.)[^\\s\$.?#].[^\\s]*)'), "link")
        .replaceAll(" ", "")
        .replaceAll("-", "")
        .replaceAll(".", "")
        .replaceAll(",", "")
        .replaceAll("(", "")
        .replaceAll(")", "")
        .replaceAll("O", "0")
        .replaceAll("О", "0")
        .replaceAll("life", " ")
        .replaceAll("l", "1")
        .replaceAll("З", "3")
        .replaceAll("I", "1");

    print('findInText: $findInText');

    while (pattern.allMatches(findInText).isNotEmpty) {
      pattern.allMatches(findInText).forEach((element) {
        var finalNumber = element.group(0);
        if (finalNumber != null) {
          finalNumber = finalNumber.replaceAll(RegExp('[^0-9]'), '');
          findInText = findInText.replaceAll(finalNumber, '');
          if (finalNumber.startsWith('375')) {
            finalNumber = '+' + finalNumber;
          }
          phone.add(finalNumber);
        }
      });
    }

    while (patternNotFull.allMatches(findInText).isNotEmpty) {
      patternNotFull.allMatches(findInText).forEach((element) {
        var finalNumber = element.group(0);
        if (finalNumber != null) {
          finalNumber = finalNumber.replaceAll(RegExp('[^0-9]'), '');
          findInText = findInText.replaceAll(finalNumber, '');
          phone.add("+375" + finalNumber);
        }
      });
    }

    fullItem = FullItem(
        title: title,
        description: descriptionFullHtml,
        descriptionShort: descriptionShort,
        srcUrls: srcUrls,
        price: price,
        isTorg: priceTorg,
        city: city,
        link: link,
        countUp: countUp,
        afterUp: afterUp,
        numbersPhone: phone.toList(),
        nameAuthor: nameAuthor,
        avatarAuthor: authorSrc,
        allAdsAuthor: allAds,
        linkWriteAuthor: linkWriteAuthor);

    print('FullItem_idProfileAuthor: $allAds');
  } catch (e, stacktrace) {
    print('FullItem_error: $e');
    print('FullItem_stacktrace: $stacktrace');
    throw ExceptionTypeErrorParse();
  }
  return fullItem;
}

String _getUrlImg(String raw) {
  int start = raw.indexOf("src=\"") + 5;
  int finish = raw.indexOf("\"", start);
  return raw.substring(start, finish);
}

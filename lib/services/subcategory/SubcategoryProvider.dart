import 'package:baraholka/data/model/Subcategory.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;


const String _kStartURL = 'baraholka.onliner.by';

class SubcategoryProvider {
  Future<List<Subcategory>> getSubcategory(int idCategory) async {
    final response = await Future.delayed(Duration(milliseconds: 500), () {
      return http.get(Uri.https(_kStartURL, ''));
    });

    if (response.statusCode == 200) {
      return _getListSubcategory(response.body, idCategory);
    } else {
      throw Exception();
    }
  }

  List<Subcategory> _getListSubcategory(String body, int idCategory) {
    List<Subcategory> list = [];
    try {
      var document = parse(body);
      Element mainTitle =
      document.querySelectorAll("div.cm-onecat")[idCategory];
      List<Element> subTitles = mainTitle.querySelectorAll("ul>li>[href]");
      subTitles.forEach((element) {
        String title = element.text;
        String? link = element.attributes['href']?.substring(1);
        list.add(Subcategory(name: title, link: link != null ? link : ''));
      });
    } catch (e) {
      print(e.toString());
    }

    return list;
  }
}

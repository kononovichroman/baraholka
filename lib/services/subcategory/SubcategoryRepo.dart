import 'package:baraholka/data/model/Subcategory.dart';

import 'SubcategoryProvider.dart';

class SubcategoryRepo {
  SubcategoryProvider _subcategoryProvider = SubcategoryProvider();

  Future<List<Subcategory>> getAllSubcategory(int idCategory) =>
      _subcategoryProvider.getSubcategory(idCategory);
}
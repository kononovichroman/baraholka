import 'dart:convert';

import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:shared_preferences/shared_preferences.dart';

const kSharedKey = 'sharedKeyFilter';

class FilterRepo {
  Filter getEmptyFilter() => Filter(
      category: Category.ALL,
      region: Region.ALL,
      sort: Sort.ACTUAL,
      onlyTitle: false);

  Future<Filter> getSaveFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? filterString = prefs.getString(kSharedKey);
    if (filterString != null) {
      return Filter.fromJson(jsonDecode(filterString));
    } else {
      return getEmptyFilter();
    }
  }

  setSaveFilter(Filter filter) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String strFilter = jsonEncode(filter.toJson());
    prefs.setString(kSharedKey, strFilter);
  }

  bool isDefaultFilter(Filter filter) {
    return filter == getEmptyFilter();
  }
}

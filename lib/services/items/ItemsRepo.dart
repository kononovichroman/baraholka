import 'package:baraholka/data/model/filter/Filter.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/ext/Pair.dart';

import 'ItemsAPIProvider.dart';

class ItemsRepo {
  ItemsAPIProvider _itemsProvider = ItemsProvider();

  Future<Pair<List<Item>, int?>> searchItem(
      String textSearch, int nextPage, Filter filter) {
    String region = _getRegionForRequest(filter.region);
    String sort = _sortForRequest(filter.sort);
    String? suffixPrice = _suffixPriceForRequest(filter.sort);
    String category = filter.category.index.toString();
    bool onlyTitle = filter.onlyTitle;
    return _itemsProvider.searchItems(
        textSearch, category, sort, suffixPrice, region, nextPage, onlyTitle);
  }

  Future<Pair<List<Item>, int?>> getAllItems(
      int idCategory, int nextPage, Filter filter) {
    String region = _getRegionForRequest(filter.region);
    String sort = _sortForRequest(filter.sort);
    String? suffixPrice = _suffixPriceForRequest(filter.sort);
    String category = filter.category.index.toString();
    return _itemsProvider.getAllItemsInSubcategory(
        idCategory, category, sort, suffixPrice, region, nextPage);
  }

  String _getRegionForRequest(Region region) {
    String finalStr;
    switch (region) {
      case Region.ALL:
        finalStr = 'region=city=0';
        break;
      case Region.MINSK:
        finalStr = 'region=city=1';
        break;
      case Region.MINSK_REGION:
        finalStr = 'region=region=349';
        break;
      case Region.BREST_REGION:
        finalStr = 'region=region=249';
        break;
      case Region.VITEBSK_REGION:
        finalStr = 'region=region=272';
        break;
      case Region.GOMEL_REGION:
        finalStr = 'region=region=304';
        break;
      case Region.GRODNO_REGION:
        finalStr = 'region=region=330';
        break;
      case Region.MOGILEV_REGION:
        finalStr = 'region=region=377';
        break;
    }
    return finalStr;
  }

  String _sortForRequest(Sort sort) {
    String finalStr;
    switch (sort) {
      case Sort.PRICE_ASCENDING:
        finalStr = 'price';
        break;
      case Sort.PRICE_DESCENDING:
        finalStr = 'price';
        break;
      case Sort.ACTUAL:
        finalStr = 'up';
        break;
      case Sort.NEW:
        finalStr = 'created';
        break;
    }
    return finalStr;
  }

  String? _suffixPriceForRequest(Sort sort) {
    String? finalStr;
    switch (sort) {
      case Sort.PRICE_ASCENDING:
        finalStr = 'asc';
        break;
      case Sort.PRICE_DESCENDING:
        finalStr = 'desc';
        break;
      case Sort.ACTUAL:
        break;
      case Sort.NEW:
        break;
    }
    return finalStr;
  }
}

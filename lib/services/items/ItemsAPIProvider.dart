import 'package:baraholka/data/model/item/Item.dart';
import 'package:baraholka/data/model/item/TypeItem.dart';
import 'package:baraholka/ext/ExceptionType.dart';
import 'package:baraholka/ext/Pair.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

const String _kStartURL = 'baraholka.onliner.by';
const String _kPathURL = '/viewforum.php';
const String _kSearchPathURL = '/search.php';
const String _kTagImportant = '1';
const String _kTagSell = '2';
const String _kTagBuy = '3';
const String _kTagSwap = '4';
const String _kTagService = '5';
const String _kTagRent = '6';
const String _kTagClose = '7';
const String _kStartLinkTopic = 'https://baraholka.onliner.by/viewtopic.php?t=';

abstract class ItemsAPIProvider {
  Future<Pair<List<Item>, int?>> getAllItemsInSubcategory(
      int idSubCategory,
      String category,
      String sort,
      String? suffixPrice,
      String region,
      int countAds);

  Future<Pair<List<Item>, int?>> searchItems(
      String query,
      String category,
      String sort,
      String? suffixPrice,
      String region,
      int countAds,
      bool onlyTitle);
}

class ItemsProvider extends ItemsAPIProvider {
  @override
  Future<Pair<List<Item>, int?>> getAllItemsInSubcategory(
      int idSubCategory,
      String category,
      String sort,
      String? suffixPrice,
      String region,
      int countAds) async {
    Map<String, dynamic> queryParams;

    if (suffixPrice != null) {
      queryParams = {
        'f': idSubCategory.toString(),
        'cat': category,
        'sk': sort,
        'sd': suffixPrice,
        'start': countAds.toString(),
      };
    } else {
      queryParams = {
        'f': idSubCategory.toString(),
        'cat': category,
        'sk': sort,
        'start': countAds.toString(),
      };
    }

    var uri = Uri.https(_kStartURL, _kPathURL, queryParams);

    final response = await Future.delayed(Duration(milliseconds: 500), () {
      return http.get(uri, headers: {'Cookie': region});
    });

    if (response.statusCode == 200) {
      return _getListItems(response.body);
    } else {
      print("Exception");
      throw Exception();
    }
  }

  @override
  Future<Pair<List<Item>, int?>> searchItems(
      String query,
      String category,
      String sort,
      String? suffixPrice,
      String region,
      int countAds,
      bool onlyTitle) async {
    Map<String, dynamic> queryParams;

    if (suffixPrice != null) {
      queryParams = {
        'q': query,
        'cat': category,
        'by': sort,
        'sort': suffixPrice,
        'start': countAds.toString(),
        'topicTitle': onlyTitle ? '1' : '0'
      };
    } else {
      queryParams = {
        'q': query,
        'cat': category,
        'by': sort,
        'start': countAds.toString(),
        'topicTitle': onlyTitle ? '1' : '0'
      };
    }

    var uri = Uri.https(_kStartURL, _kSearchPathURL, queryParams);
    final response = await Future.delayed(Duration(milliseconds: 500), () {
      return http.get(uri, headers: {'Cookie': region});
    });

    if (response.statusCode == 200) {
      return _getListItems(response.body);
    } else {
      print("Exception");
      throw Exception();
    }
  }

  Pair<List<Item>, int?> _getListItems(String body) {
    List<Item> list = [];
    int? intNext;
    try {
      var document = parse(body);

      String? urlNext;
      if (document
              .querySelector('p.search-not-found')
              ?.text
              .contains("Не найдено") ==
          true) {
        throw Exception(ExceptionTypeNotFound);
      }

      List<Element> itemListElement = document.querySelectorAll('tr>td>table');
      List<Element> itemPriceListElement =
          document.querySelectorAll('tr>td.cost');
      urlNext = document.querySelector('.page-next a')?.attributes['href'];
      if (urlNext?.isNotEmpty == true) {
        urlNext = urlNext?.substring(urlNext.indexOf('start=') + 6);
        intNext = int.tryParse(urlNext!);
      }

      itemListElement.asMap().forEach((index, element) {
        String? title = element.querySelector('.wraptxt a')?.text;
        String? url = element.querySelector('.wraptxt a')?.attributes['href'];
        int idItem = 0;
        if (url != null) {
          idItem = int.parse(url.substring(url.indexOf('?t=') + 3));
          url = _kStartLinkTopic + idItem.toString();
        }
        String? price =
            itemPriceListElement[index].querySelector('.price-primary')?.text;
        bool priceTorg = false;
        if (price != null &&
            itemPriceListElement[index].querySelector('.cost-torg')?.text !=
                null) {
          priceTorg = itemPriceListElement[index]
                      .querySelector('.cost-torg')!
                      .text
                      .trim()
                      .length >
                  2
              ? true
              : false;
        }

        price = price != null ? price : "Цена не указана";

        String? city =
            element.querySelector('.ba-signature strong')?.text ?? '';
        String? src = element.querySelector('img')?.attributes['src'];
        bool isPremium;
        if (src?.contains('large') == true) {
          isPremium = true;
        } else {
          src = src?.replaceAll('icon', 'icon_large');
          isPremium = false;
        }

        String? description = element.querySelector('p.ba-description')?.text;
        description = description?.replaceAll('\n', ' ');
        if (description == null) {
          description = 'Краткое описание отсутсвует';
        }

        String? labelString = element.querySelector('div.ba-label')?.className;
        TypeItem type;
        if (labelString != null) {
          if (labelString.contains(_kTagImportant)) {
            type = TypeItem.IMPORTANT;
          } else if (labelString.contains(_kTagSell)) {
            type = TypeItem.SELL;
          } else if (labelString.contains(_kTagBuy)) {
            type = TypeItem.BUY;
          } else if (labelString.contains(_kTagSwap)) {
            type = TypeItem.SWAP;
          } else if (labelString.contains(_kTagService)) {
            type = TypeItem.SERVICE;
          } else if (labelString.contains(_kTagRent)) {
            type = TypeItem.RENT;
          } else if (labelString.contains(_kTagClose)) {
            type = TypeItem.CLOSE;
          } else {
            type = TypeItem.CLOSE;
          }
        } else {
          type = TypeItem.CLOSE;
        }

        if (title != null && src != null && url != null) {
          list.add(Item(
              title: title,
              description: description,
              srcUrl: src,
              price: price,
              city: city,
              link: url,
              type: type,
              isPremium: isPremium,
              isTorg: priceTorg,
              id: idItem));
        } else {
          // throw Exception("Null itemListElement: ${title != null}, ${description != null}, ${src != null}, ${city != null}, ${url != null} ");
        }
      });
    } catch (e) {
      print("Provider" + e.toString());
    }

    return Pair(first: list, second: intNext);
  }
}

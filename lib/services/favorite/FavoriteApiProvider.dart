import 'package:baraholka/core/db/FavoriteItem.dart';
import 'package:baraholka/core/db/FavoriteItemType.dart';
import 'package:baraholka/data/model/item/Item.dart';
import 'package:hive/hive.dart';

const favoriteBox = 'favorites';

class FavoriteApiProvider {
  Future<bool> isFavorite(String link) async {
    return Hive.box<FavoriteItem>(favoriteBox).get(link) != null;
  }

  Future<bool> addItemToFavorite(Item item) async {
    FavoriteItem _favoriteItem = FavoriteItem(
        title: item.title,
        description: item.description,
        srcUrl: item.srcUrl,
        price: item.price,
        isTorg: item.isTorg,
        city: item.city,
        link: item.link,
        isPremium: item.isPremium,
        type: item.type.convertType,
        id: item.id);
    var favoritesBox = Hive.box<FavoriteItem>(favoriteBox);
    favoritesBox.put(item.link, _favoriteItem);
    return true;
  }

  Future<void> removeItemFromFavorite(Item item) async {
    var favoritesBox = Hive.box<FavoriteItem>(favoriteBox);
    favoritesBox.delete(item.link);
  }
  Future<void> removeAllFavoritesItems() async {
    var favoritesBox = Hive.box<FavoriteItem>(favoriteBox);
    favoritesBox.deleteFromDisk();
  }

  Future<List<FavoriteItem>> getAllFavoritesItems() async {
    var favoritesBox = await Hive.openBox<FavoriteItem>(favoriteBox);
    return favoritesBox.values.toList();
  }
}

import 'package:baraholka/core/db/FavoriteItem.dart';
import 'package:baraholka/data/model/item/Item.dart';

import 'FavoriteApiProvider.dart';
import 'FavoriteRepoAPI.dart';


class FavoriteRepo extends FavoriteRepoAPI{
  FavoriteApiProvider _favoriteApiProvider =
      FavoriteApiProvider();


  @override
  Future<bool> isFavorite(String link) {
    return _favoriteApiProvider.isFavorite(link);
  }
  @override
  Future<void> addToFavorite(Item item) {
    return _favoriteApiProvider.addItemToFavorite(item);
  }

  @override
  Future<void> removeFromFavorite(Item item) {
    return _favoriteApiProvider.removeItemFromFavorite(item);
  }

  @override
  Future<List<FavoriteItem>> getAllFavoritesItems() {
    return _favoriteApiProvider.getAllFavoritesItems();
  }

  @override
  Future<void> deleteAll() {
    return _favoriteApiProvider.removeAllFavoritesItems();
  }


}

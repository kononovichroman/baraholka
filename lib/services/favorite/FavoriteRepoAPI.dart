import 'package:baraholka/core/db/FavoriteItem.dart';
import 'package:baraholka/data/model/item/Item.dart';

abstract class FavoriteRepoAPI {
  Future<bool> isFavorite(String link);

  Future<void> addToFavorite(Item item);

  Future<void> removeFromFavorite(Item item);

  Future<List<FavoriteItem>> getAllFavoritesItems();

  Future<void> deleteAll();

}

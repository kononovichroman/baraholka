import 'package:flutter/material.dart';
class CustomColors {
  CustomColors._();

  static const Color kMainBlueColor = Color(0xff2478FF);
  static const Color kMainGrayColor = Color(0xffF1F2F6);
  static const Color kWhiteColor = Color(0xffffffff);
  static const Color kIconGrayColor = Color(0xff9E9FA0);


  static const  Color kChipTypeItemImportant = Color(0xffA73301);
  static const  Color kChipTypeItemBuy = Color(0xff36B445);
  static const  Color kChipTypeItemSell = Color(0xffF54443);
  static const  Color kChipTypeItemClose = Color(0xff808080);
  static const  Color kChipTypeItemRent = Color(0xffF66633);
  static const  Color kChipTypeItemService = Color(0xff3CB6D3);
  static const  Color kChipTypeItemSwap = Color(0xffA441C9);
}


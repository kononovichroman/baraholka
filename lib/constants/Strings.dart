class Strings {
  static const String baraholkaTitle = 'baraholka';
  static const String textFieldHintText = 'Поиск на барахолке';
  static const String favoriteScreenAppBar = 'Избранные объявления';
  static const String favoriteScreenEmptyTitle = 'Пусто';
  static const String favoriteScreenEmptyDesc = 'Вы можете добавить объявление в закладки на экране самого объявления';

  static const String filterScreenAppBarTitle = 'Сортировка и фильтр';
  static const String filterScreenFilterSectionTitle = 'Фильтр';
  static const String filterScreenCategorySectionTitle = 'Категория';
  static const String filterScreenRegionSectionTitle = 'Регион';
  static const String filterScreenSortSectionTitle = 'Сортировка';
  static const String filterScreenOnlyTitleSection = 'Только в заголовках';
  static const String filterScreenApplyBtn = 'Применить';

  static const String fullItemScreenAllAds = 'Все объявления продавца';
  static const String fullItemScreenWriteSeller = 'Написать продавцу';
  static const String fullItemScreenCallBtn = 'Позвонить';
  static const String fullItemScreenCarouselIndicator = ' / ';

  static const String fullItemScreenModalActionCancel = 'Отменить';
  static const String fullItemScreenModalActionTitle = 'Выберите номер телефона';

  static const String fullImageScreenCloseBtnCupertino = 'Закрыть';
  static const String fullImageScreenAppBarTitle = ' из ';

  static const String favoriteScreenDialogMsg = 'Вы действительно хотите удалить все избранные объявления?';
  static const String favoriteScreenDialogActionBtn = 'Удалить';
  static const String favoriteScreenDialogCancelBtn = 'Отмена';


  static const String searchScreenStubTitle = 'Введите запрос';
  static const String searchScreenStubDesc = 'Например, MacBook Pro';
  static const String searchScreenNotFoundTitle = 'Ничего не найдено';
  static const String searchScreenNotFoundDesc = 'Попробуйте поискать что-нибудь еще';

  static const String pageClosedTitle = 'Ошибка';
  static const String pageClosedDesc = 'Объявление не найдено (возможно уже не актуально)';

  static const String errorNetworkWidgetTitle = 'Что-то пошло не так';
  static const String errorNetworkWidgetDesc = 'Вероятно, соединение с интернетом прервано';
  static const String errorNetworkWidgetRetryBtn = 'Попробовать еще раз';

  static const String newPageErrorIndicatorDesc = 'При загрузке данных произошла ошибка';

  static const String chipTypeImportant = 'Важно!';
  static const String chipTypeBuy = 'Куплю';
  static const String chipTypeSell = 'Продам';
  static const String chipTypeClose = 'Закрыто';
  static const String chipTypeRent = 'Аренда';
  static const String chipTypeService = 'Услуга';
  static const String chipTypeSwap = 'Обмен';

  static const String textTorg = 'торг';
  static const String textPriceNotSpecified = 'Цена не указана';

  static const String bottomNavBarCategory = 'Категории';
  static const String bottomNavBarSearch = 'Поиск';
  static const String bottomNavBarFavorite = 'Избранные';
}